<?php

$current_path = getcwd(); //Current app path

$drupal_root = $current_path . '/../';

define('DRUPAL_ROOT', $drupal_root);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if(!empty($_POST['id_code'])) {
    $entered_id_code = $_POST['id_code'];
} else {
    $response = json_encode(array('status' => 'AUTHENTICATION_DENIED'));
    echo $response;
    exit();
}

//@$_SERVER['REDIRECT_SSL_CLIENT_VERIFY'] = 'SUCCESS'; // REMOVE AFTER TESTING
//@$_SERVER['REDIRECT_SSL_CLIENT_S_DN_CN'] = 'PERE,EES,123456789'; // REMOVE AFTER TESTING
if(@$_SERVER['SSL_CLIENT_VERIFY'] == 'SUCCESS'
    && @$_SERVER['SSL_CLIENT_S_DN_CN']) {
    list($lastname, $firstname, $id_code) = explode(',', $_SERVER['SSL_CLIENT_S_DN_CN']);
        if ($entered_id_code == $id_code){

            $hash = hash('md5', $id_code . time());
            cache_set($hash, $id_code, 'cache', 60);
            header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/dds/verify?i=' . $hash);
        } else {

            drupal_set_message('Sisestatud isikukood ei ühtinud id kaardil oleva isikukoodiga!');
            header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/korraldaja');
        }
    } else {
    $response = json_encode(array('status' => 'AUTHENTICATION_DENIED'));
    print $response;
    exit();
}