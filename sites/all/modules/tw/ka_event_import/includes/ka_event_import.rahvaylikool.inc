<?php

function ka_event_import_rahvaylikool(){
	//kpr($nodes);
	//https://www.minu.rahvaylikool.ee/kultuuriaken
	//kellad ja kuupäevasid ei saa sisestada
	$response_data_string = drupal_http_request('https://www.minu.rahvaylikool.ee/kultuuriaken')->data;
	$response_array = json_decode($response_data_string);
	
	kpr($response_array);
	
	
	foreach ($response_array as $item) {
		$step['step_1'] = array(
			'category' => array(
				'field_category' => '21',
				//'field_subcategory' => 	get_event_category($show['Genres'], $movie_event_terms),
			),
		);
		$step['step_2'] = array(
			'introduction_languages' => array(
				'et' => 'et',
				'ru' => 0,
				'en' => 0
			),
			'et' => array(
				'field_title' => $item->name,
				'field_summary' => $item->teaser,
				'field_body' => $item->body,
				//'field_additional_info_link' => $item->link,
			),
		);
		$step['step_3'] = array(
			'groups' => get_group_info($item->groups)
		);
		
		/*$excisting_gallery = (isset($existing_api_events[$show['ID']]) && $existing_api_events[$show['ID']]->field_gallery[LANGUAGE_NONE]) ? $existing_api_events[$show['ID']]->field_gallery[LANGUAGE_NONE] : NULL;
		
		$gallery = get_event_images($show['Gallery'], $excisting_gallery);
		
		$step['step_4'] = array(
			'gallery' => $gallery
		);
		$step['step_5'] = array(
			'left' => array(
				'field_organizer_name' => '',
				'field_organizer_www' => '',
				'field_organizer_address' => '',
			),
			'right' => array(
				'field_organizer_contact' => '',
				'field_organizer_email' => '',
				'field_organizer_phone' => '',
			),
			'registration' => array(
				''
			),
		);
		$step['node_type'] = 'activity';
		$step['field_api_provider'] = '';
		$step['field_api_id'] = '';
		if(isset($existing_api_events[$show['ID']]) && $existing_api_events[$show['ID']]){
			$step['existing_node'] = $existing_api_events[$show['ID']];
		}*/
		
		$full_event_info[] = $step;
		//kpr($item->category_name);
	}
	kpr($full_event_info);
}

function get_group_info($group_array) {
	//kpr($group_array);
	$groups = array();
	foreach($group_array as $item){
		kpr($item->info);
		$group = array(
			'name' => $item->name,
			'age_groups' => array(
				'children' => 0,
				'adults' => 0,
			),
			'age_range' => array(
				'from' => '',
				'to' => '',
			),
			'schedule' => array(
				'type' => 'regular',
				'regular' => array(
					'period_from' => str_replace('-', '.', $item->start_date),
					'period_to' => str_replace('-', '.', $item->end_date),
				),
			),
			'price' => array(
				'type' => 'single_payment',
				'single_payment' => $item->price,
			),
		);
		$groups[] = $group;
	}
	return $groups;
	/*$groups['group'] => array(
			array(
				'name' => t('grupp1'),
				'age_groups' => array(
					'children' => 0,
					'adults' => 0,
				),
				'age_range' => array(
					'from' => '',
					'to' => '',
				),
				'schedule' => array(
					'type' => 'regular',
					'regular' => array(
						'period_from' => '',
						'period_to' => '',
					),
				),
				'price' => array(
					'type' => 'single_payment',
					'single_payment' => '',
				),
			),
		)*/
}