<?php
function ka_event_import_vanemuine(){
	$full_event_info = array();
	$target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
	$target_groups_assoc = get_taxonomy_assoc($target_groups);
	$existing_api_events = get_nodes_by_api('vanemuine', 'event');
	$theatre_event_terms = taxonomy_get_children(1);
	//http://www.vanemuine.ee/moodulid/kultuuriaken/index.php
	$response_data_string = drupal_http_request('http://www.vanemuine.ee/moodulid/kultuuriaken/index.php')->data;
	$response_array = response_data_string_to_array($response_data_string);
	
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'file')
				->entityCondition('bundle', 'image')
	  		->fieldCondition('field_source_url', 'url', 'vanemuine');
	$result = key($query->execute()['file']);
	
	$vanemuine_default_image = file_load($result);
	
	$i = 0;
	foreach ($response_array['lavastus'] as $lavastus) {
  	$step = array();
  	$step['step_1'] = array(
			'introduction_languages' => array(
				'et' => 'et',
				'ru' => 0,
				'en' => 0
			),
			'et' => array(
				'field_title' => iconv(mb_detect_encoding($lavastus['title'], mb_detect_order(), true), "UTF-8", $lavastus['title']),
				'field_summary' => ($lavastus['intro']) ? $lavastus['intro'] : '',
				'field_body' => ($lavastus['text_clean'] && !empty($lavastus['text_clean'])) ? $lavastus['text_clean'] : strip_tags($lavastus['text']),
				'field_additional_info_link' => $lavastus['url'],
		    ),
		);
		$step['step_2'] = array(
			'category' => array(
				'field_category' => array('1' => '1'),
				'field_subcategory' => 	get_event_category($lavastus['genre'], $theatre_event_terms),
			),
			'field_target_groups' => get_target_group('', $target_groups_assoc),
      	);
  	$step['step_3'] = array(
			'event_date_location' => parse_event_date_location_vanemuine($lavastus['shows']),
			'event_tickets' => array(
				'price' => array(
					'field_event_ticket_type' => 'between',
	  			'field_event_ticket_price' => '',
	  			'field_event_ticket_price_from' => '5',
	  			'field_event_ticket_price_to' => '23',
				),
				'availability' => array(
					'field_event_ticket_availability' => 'link',
					'field_event_ticket_link' => $lavastus['url'],
					'field_event_ticket_other' => '',
				),
				'additional' => array(
					'et' => 'http://www.vanemuine.ee/repertuaarid/piletiinfo-2/',
					'ru' => '',
					'en' => '',
				),
			),
  	);
  	//kpr($step['step_2']['category']['field_subcategory']);
      	
  	$step['step_4'] = array();
  	$excisting_gallery = (isset($existing_api_events[$lavastus['id']]) && !empty($existing_api_events[$lavastus['id']]->field_gallery)) ? $existing_api_events[$lavastus['id']]->field_gallery[LANGUAGE_NONE] : NULL;
  	
  	if(!empty($lavastus['promopilt'][0])){
  		if($excisting_gallery && ($excisting_gallery[0]['field_source_url'][LANGUAGE_NONE][0]['url'] == $lavastus['promopilt'][0]['@attributes']['url'])){
  			$img = $excisting_gallery[0];
  			$step['step_4']['gallery']['image_preview']['images'][$img['fid']] = array('file' => $img);
  		}else{
  			$img = ka_download_image_link($lavastus['promopilt'][0]['@attributes']['url']);
  			$step['step_4']['gallery']['image_preview']['images'][$img->fid] = array('file' => $img);
  		}
  		$step['step_4']['gallery']['image_preview']['main_image'] = (string) key($step['step_4']['gallery']['image_preview']['images']);
		}else{
			$step['step_4']['gallery']['image_preview']['images'][$vanemuine_default_image->fid] = array('file' => $vanemuine_default_image);
			$step['step_4']['gallery']['image_preview']['main_image'] = (string) key($step['step_4']['gallery']['image_preview']['images']);
		}
    		
    $step['step_5'] = array(
  		'left' => array(
			'field_organizer_name' => 'Teater Vanemuine',
			'field_organizer_www' => 'http://www.vanemuine.ee/',
			'field_organizer_address' => 'Vanemuise 6, 51010 Tartu',
			),
			'right' => array(
				'field_organizer_contact' => 'Teater Vanemuine',
				'field_organizer_email' => $lavastus['epost'],
				'field_organizer_phone' => '+372 '. $lavastus['telefon'],
	 		),
  	);
  	$step['node_type'] = 'event';
  	$step['field_api_provider'] = 'vanemuine';
  	$step['field_api_id'] = $lavastus['id'];
    	
    if(isset($existing_api_events[$lavastus['id']]) && $existing_api_events[$lavastus['id']]){
    	$step['existing_node'] = $existing_api_events[$lavastus['id']];
  	}
    	
    $i++;
  	if(!empty($step['step_3']['event_date_location'])) $full_event_info[] = $step;
  	/*if($i == 10){
  		break;
  	}*/
	}
	
	//kpr($full_event_info);
	return $full_event_info;
}

function parse_event_date_location_vanemuine($info){
	$event_dates = array();
	
	if(!empty($info)){
		if(key($info['show']) === 0){
			foreach ($info['show'] as $value) {
				
				$et = strtotime($value['time']); // Add 5 minute
				$ed = strtotime($value['date']);
				
				//if(strtotime($ed) <= time()) continue;
				
				
				$end_time = date("H:i", $et);
				$end_date = date('d.m.Y', $ed);
				
				$event_dates[] = array(
					'date' => array(
						'start' => array(
							'field_event_date_from' => $value['date'],
							'field_event_time_from' => $value['time'],
						),
						'end' => array(
							'field_event_date_to' => $end_date,
							'field_event_time_to' => $end_time,
						),
					),
					'location' => array(
						'field_event_location' => $value['venue'],
						'field_event_address' => $value['aadress'],
					),
				);
			}
		}else{
			//if($info['show']['time'] <= time());
			$et = strtotime($info['show']['time']); // Add 5 minute
			$ed = strtotime($info['show']['date']);
			
			//if(strtotime($ed) <= time()) return;
			
			$end_time = date("H:i", $et);
			$end_date = date('d.m.Y', $ed);
			
			$event_dates[] = array(
				'date' => array(
					'start' => array(
						'field_event_date_from' => $info['show']['date'],
						'field_event_time_from' => $info['show']['time'],
					),
					'end' => array(
						'field_event_date_to' => $end_date,
						'field_event_time_to' => $end_time,
					),
				),
				'location' => array(
					'field_event_location' => $info['show']['aadress'],
					'field_event_address' => $info['show']['aadress'],
				),
			);
		}
	}
	return $event_dates;
}