<?php

function ka_event_import_ahhaa(){
	//kpr(ka_calendar_json_callback());
  //http://veeb.ahhaa.ee/epilet/kultuuriaken.php
	$response_data_string = drupal_http_request('http://veeb.ahhaa.ee/epilet/kultuuriaken.php')->data;
	$events_json = json_decode($response_data_string);
  
	kpr($events_json);
  
	foreach ($events_json as $key => $event) {
		kpr($event);
		$event_type = $key;
		kpr($key);
  	$step = array();
  	/*$step['step_1'] = array(
			'introduction_languages' => array(
				'et' => 'et',
				'ru' => 0,
				'en' => 0
			),
			'et' => array(
				'field_title' => $yritus['title'],
				'field_summary' => '',
				'field_body' => '',
				'field_additional_info_link' => $yritus['url'],
		    ),
		);
		$step['step_2'] = array(
			'category' => array(
				'field_category' => array('8' => '8'),
			),
			'field_target_groups' => get_target_group('', $target_groups_assoc),
  	);
  	$step['step_3'] = array(
			'event_date_location' => parse_event_date_location_luts($yritus),
			'event_tickets' => array(
				'price' => array(
					'field_event_ticket_type' => 'free',
				),
				'availability' => array(
					'field_event_ticket_availability' => NULL,
					'field_event_ticket_link' => '',
					'field_event_ticket_other' => '',
				),
			),
  	);
      	
  	$step['step_4'] = array();
		$excisting_gallery = (isset($existing_api_events[$yritus['id']]) && !empty($existing_api_events[$yritus['id']]->field_gallery)) ? $existing_api_events[$yritus['id']]->field_gallery[LANGUAGE_NONE] : NULL;

		$img = NULL;
		if($excisting_gallery){
		  $img = file_load($excisting_gallery[0]['fid']);
		  kpr('gallerii olemas');
		}elseif(!empty($yritus['promopilt']['@attributes']['url']) && !$img){
		  $img_url = 'https://' . str_replace(' ', '%20',$yritus['promopilt']['@attributes']['url']);
		  $img = ka_download_image_link($img_url);

		}
		if($img){
      $step['step_4']['gallery']['image_preview']['images'][$img->fid] = array('file' => $img);
		  $step['step_4']['gallery']['image_preview']['main_image'] = $img->fid;
		}
    		
    $step['step_5'] = array(
  		'left' => array(
			'field_organizer_name' => 'Tartu Linnaraamatukogu',
			'field_organizer_www' => 'https://www.luts.ee/',
			'field_organizer_address' => 'Kompanii 3/5, 51004 Tartu',
			),
			'right' => array(
				'field_organizer_contact' => 'Tartu Linnaraamatukogu',
				'field_organizer_email' => 'oskar@luts.ee',
				'field_organizer_phone' => preg_replace('/\D/', '', $yritus['contact']),
	 		),
  	);
  	$step['node_type'] = 'event';
  	$step['field_api_provider'] = 'luts';
  	$step['field_api_id'] = $yritus['id'];
    	
    if(isset($existing_api_events[$yritus['id']]) && $existing_api_events[$yritus['id']]){
    	$step['existing_node'] = $existing_api_events[$yritus['id']];
  	}
    	
  	if(!empty($step['step_3']['event_date_location'])) $full_event_info[] = $step;
	
	*/}
	
  //kpr($full_event_info);
  //return $full_event_info;
}

function parse_event_date_location_ahhaa($yritus){
  $event_dates = array();
  
  $start_date = $yritus['startdate'];
  $start_time = $yritus['starttime'];
  
  $end_date = $yritus['enddate'];
  $end_time = $yritus['endtime'];
  
  $address = $yritus['location'];
  
  $event_dates[] = array(
		'date' => array(
			'start' => array(
				'field_event_date_from' => $start_date,
				'field_event_time_from' => $start_time,
			),
			'end' => array(
				'field_event_date_to' => $end_date,
				'field_event_time_to' => $end_time,
			),
		),
		'location' => array(
			'field_event_location' => $address,
			'field_event_address' => '',
		),
	);
	
	return $event_dates;
}