<?php

function ka_event_import_luts(){
  //https://www.luts.ee/kultuuriaken/yritused/
  //https://www.luts.ee/kultuuriaken/naitused/
  #return array();
  $response_data_string = drupal_http_request('https://www.luts.ee/kultuuriaken/yritused/')->data;
  $events_array = response_data_string_to_array($response_data_string);

	$response_data_string_show = drupal_http_request('https://www.luts.ee/kultuuriaken/naitused/')->data;
	$show_array = response_data_string_to_array($response_data_string_show);
	//$show_array['naitus'] = array();
	$events_array = array_merge($events_array, $show_array);
	
	//kpr($events_array);
	//die();


  $target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
  $target_groups_assoc = get_taxonomy_assoc($target_groups);
  
  $existing_api_events = get_nodes_by_api('luts', 'event');
  foreach ($events_array as $key => $yritus) {
  	foreach($yritus as $item){
	  	$step = array();
	  	$step['step_1'] = array(
				'introduction_languages' => array(
					'et' => 'et',
					'ru' => 0,
					'en' => 0
				),
				'et' => array(
					'field_title' => $item['title'],
					'field_summary' => '',
					'field_body' => $item['text'],
					'field_additional_info_link' => $item['url'],
			    ),
			);
			$category = ($key == 'yritus') ? array('8' => '8') : array('4' => '4') ;
			$step['step_2'] = array(
				'category' => array(
					'field_category' => $category,
				),
				'field_target_groups' => get_target_group('', $target_groups_assoc),
	  	);
	  	$step['step_3'] = array(
				'event_date_location' => parse_event_date_location_luts($item),
				'event_tickets' => array(
					'price' => array(
						'field_event_ticket_type' => 'free',
					),
					'availability' => array(
						'field_event_ticket_availability' => NULL,
						'field_event_ticket_link' => '',
						'field_event_ticket_other' => '',
					),
				),
	  	);
	      	
	  	$step['step_4'] = array();
			$excisting_gallery = (isset($existing_api_events[$item['id']]) && !empty($existing_api_events[$item['id']]->field_gallery)) ? $existing_api_events[$item['id']]->field_gallery[LANGUAGE_NONE] : NULL;
			//kpr($excisting_gallery);
			$img = NULL;
			if($excisting_gallery){
			  $img = file_load($excisting_gallery[0]['fid']);
			  file_delete($img);
  		}
  		if(!empty($item['promopilt']['@attributes']['url'])){
			  $img_url = 'https://' . str_replace(' ', '%20',$item['promopilt']['@attributes']['url']);
			  $img = ka_download_image_link($img_url);
			}elseif(empty($item['promopilt']['@attributes']['url'])){
			  $img_url = 'https://' . 'www.luts.ee/images/kohvik/valisvaade.jpg';
			  $img = ka_download_image_link($img_url);
			}
  		
			if($img){
	      $step['step_4']['gallery']['image_preview']['images'][$img->fid] = array('file' => $img);
			  $step['step_4']['gallery']['image_preview']['main_image'] = $img->fid;
			}
	    		
	    $step['step_5'] = array(
	  		'left' => array(
				'field_organizer_name' => 'Tartu Linnaraamatukogu',
				'field_organizer_www' => 'https://www.luts.ee/',
				'field_organizer_address' => 'Kompanii 3/5, 51004 Tartu',
				),
				'right' => array(
					'field_organizer_contact' => 'Tartu Linnaraamatukogu',
					'field_organizer_email' => 'oskar@luts.ee',
					'field_organizer_phone' => preg_replace('/\D/', '', $item['contact']),
		 		),
	  	);
	  	$step['node_type'] = 'event';
	  	$step['field_api_provider'] = 'luts';
	  	$step['field_api_id'] = $item['id'];
	    	
	    if(isset($existing_api_events[$item['id']]) && $existing_api_events[$item['id']]){
	    	$step['existing_node'] = $existing_api_events[$item['id']];
	  	}
	    //kpr($step);
	  	if(!empty($step['step_3']['event_date_location'])) $full_event_info[] = $step;
		}
  }
	
  //kpr($full_event_info);
	return $full_event_info;
}

function parse_event_date_location_luts($yritus){
  $event_dates = array();
  
  $start_date = $yritus['startdate'];
  $start_time = $yritus['starttime'];
  
  $end_date = $yritus['enddate'];
  $end_time = $yritus['endtime'];
  
  $address = !empty($yritus['address']) ? $yritus['address'] : 'Kompanii 3/5, Tartu 51004';
  $venue = !empty($yritus['venue']) ? $yritus['venue'] : '';
  
  $event_dates[] = array(
		'date' => array(
			'start' => array(
				'field_event_date_from' => $start_date,
				'field_event_time_from' => $start_time,
			),
			'end' => array(
				'field_event_date_to' => $end_date,
				'field_event_time_to' => $end_time,
			),
		),
		'location' => array(
			'field_event_location' => $venue,
			'field_event_address' => $address,
		),
	);
	//kpr($event_dates);
	return $event_dates;
}