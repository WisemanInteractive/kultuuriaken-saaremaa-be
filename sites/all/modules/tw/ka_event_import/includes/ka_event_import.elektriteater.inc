<?php
function ka_event_import_elekter(){
	$full_event_info = array();
	
	$existing_api_events = get_nodes_by_api('elektriteater', 'event');
	
	$target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
	$target_groups_assoc = get_taxonomy_assoc($target_groups);
	
	$response_data_string = drupal_http_request('https://elektriteater.ee/wp-json/kultuuriaken/events')->data;
	$response_array = json_decode($response_data_string);
	
	$i = 0;
	
	foreach ($response_array->list as $item) {
	  $step = array();
	    $ticket_link = 'https://elektriteater.ee/';
	  	if(!empty($item->events) && isset($item->events[0])) $ticket_link = $item->events[0]->link;
		  
	  	$step['step_1'] = array(
				'introduction_languages' => array(
					'et' => 'et',
					'ru' => 0,
					'en' => 0
				),
				'et' => array(
					'field_title' => html_entity_decode(htmlspecialchars_decode($item->title)),
					'field_summary' => html_entity_decode(htmlspecialchars_decode($item->summary)),
					'field_body' => html_entity_decode(htmlspecialchars_decode(strip_tags($item->content))),
					'field_additional_info_link' => $ticket_link,
			    ),
			);
			
			$step['step_2'] = array(
				'category' => array(
					'field_category' => array('2' => '2'),
					//'field_subcategory' => 	array(),/*get_event_category($lavastus['genre'], $theatre_event_terms),*/
				),
				'field_target_groups' => get_target_group('K-14', $target_groups_assoc),
	  	);
	  	
	  	$price_obj = [
  			'field_event_ticket_type' => 'free',
  		];
	  	
	  	if(is_object($item->price)){
	  		$price_obj = [
					'field_event_ticket_type' => 'fixed',
	  			'field_event_ticket_price' => $item->price->fixed,
				];
	  	}
	  	
	  	$step['step_3'] = array(
				'event_date_location' => parse_event_date_location_elektriteater($item->events),
				'event_tickets' => array(
					'price' => $price_obj,
					'availability' => array(
						'field_event_ticket_availability' => 'link',
						'field_event_ticket_link' => $ticket_link,
						'field_event_ticket_other' => '',
					),
					'additional' => array(
						'et' => 'https://elektriteater.ee/kinost/#piletid',
						'ru' => '',
						'en' => '',
					),
				),
	  	);
	  	
	  	$excisting_gallery = (isset($existing_api_events[$item->id]) && !empty($existing_api_events[$item->id]->field_gallery)) ? $existing_api_events[$item->id]->field_gallery[LANGUAGE_NONE] : array();
	  	$gallery = get_event_images_elekter($item->images, $excisting_gallery);
	  	
	  	$step['step_4'] = array(
	  		'gallery' => $gallery
	  	);
	  	
	  	$step['step_5'] = array(
	  		'left' => array(
				'field_organizer_name' => $item->organizer_info->organizer_name,
				'field_organizer_www' => $item->organizer_info->organizer_link->url,
				'field_organizer_address' => 'Tartu Ülikooli kirik (Jakobi 1)',
				),
				'right' => array(
					'field_organizer_contact' => $item->organizer_info->organizer_contact,
					'field_organizer_email' => $item->organizer_info->organizer_email,
					'field_organizer_phone' => '+372 ' . $item->organizer_info->organizer_phone,
		 		),
	  	);
	  	$step['node_type'] = 'event';
	  	$step['field_api_provider'] = 'elektriteater';
	  	$step['field_api_id'] = $item->id;
	  	
	  	if(isset($existing_api_events[$item->id])){
	  		$step['existing_node'] = $existing_api_events[$item->id];
	  	}
	  	
	  	$full_event_info[] = $step;
	}
	
	return $full_event_info;
}

function parse_event_date_location_elektriteater($info){
	$event_dates = array();
	foreach($info as $event){
  	$st = $event->start;
		$et = $event->end;
		//if(strtotime($ed) <= time()) continue;
		
		$start_time = date("H:i", $st);
		$start_date = date('d.m.Y', $st);
		
		$end_time = date("H:i", $et);
		$end_date = date('d.m.Y', $et);
		
		$event_dates[] = array(
			'date' => array(
				'start' => array(
					'field_event_date_from' => $start_date,
					'field_event_time_from' => $start_time,
				),
				'end' => array(
					'field_event_date_to' => $end_date,
					'field_event_time_to' => $end_time,
				),
			),
			'location' => array(
				'field_event_location' => 'Elektriteater',
				'field_event_address' => $event->location_string,
			),
		);
	}
	
	return $event_dates;
}

function get_event_images_elekter($images, $excisting_gallery){
	$images_array = array();
	$ex_imgs = array();
	//kpr($excisting_gallery);
	
	if(!empty($excisting_gallery)){
		foreach($excisting_gallery as $imgs){
			$ex_imgs[] = $imgs['field_source_url'][LANGUAGE_NONE][0]['url'];
		}
	}
	
	if(!empty($images)){
		foreach($images as $image){
			if(!in_array($image, $ex_imgs)){
				if($img = ka_download_image_link($image)){
					$images_array['image_preview']['images'][$img->fid]['file'] = $img;
				}
			}else{
				foreach($excisting_gallery as $excisting_image){
					$images_array['image_preview']['images'][$excisting_image['fid']] = $excisting_image;
				}
			}
		}
		
		$last_key  = (string) key($images_array['image_preview']['images']);
		
		if($last_key){
			$images_array['image_preview']['main_image'] = $last_key;
		}
	}
	return $images_array;
}

function encodeToUtf8($string) {
     return mb_convert_encoding($string, "UTF-8", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15, HTML-ENTITIES", true));
}