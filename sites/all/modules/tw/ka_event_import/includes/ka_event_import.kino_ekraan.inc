<?php

function ka_event_import_ekraan(){
	
	$existing_api_events = get_nodes_by_api('kino_ekraan', 'event');
	
	$target_groups = taxonomy_get_tree(TARGET_GROUPS_VID);
	$target_groups_assoc = get_taxonomy_assoc($target_groups);
	$movie_event_terms = taxonomy_get_children(2);

  
	$full_event_info = array();
	$response_data_string = drupal_http_request('https://www.forumcinemas.ee/xml/Events/?area=1005&includeGallery=true')->data;
	$response_array = response_data_string_to_array($response_data_string);

	$i = 0;
	foreach ($response_array['Event'] as $show) {
  		$step = array();
		$event_dates_xml = drupal_http_request('https://www.forumcinemas.ee/xml/Schedule/?area=1005&nrOfDays=31&eventId=' . $show['ID'])->data;
		$event_dates = response_data_string_to_array($event_dates_xml);
	  	
	  	if(empty($event_dates['Shows'])) continue;
	  	$step['step_1'] = array(
				'introduction_languages' => array(
					'et' => 'et',
					'ru' => 0,
					'en' => 0
				),
				'et' => array(
					'field_title' => $show['Title'],
					'field_summary' => $show['ShortSynopsis'],
					'field_body' => $show['Synopsis'],
					'field_additional_info_link' => $show['EventURL'],
				),
			);
	  	$step['step_2'] = array(
				'category' => array(
					'field_category' => array('2' => '2'),
					'field_subcategory' => 	get_event_category($show['Genres'], $movie_event_terms),
				),
				'field_target_groups' => get_target_group($show['RatingLabel'], $target_groups_assoc),
	  	);
	  	$step['step_3'] = array(
				'event_date_location' => parse_event_date_location($event_dates),
				'event_tickets' => array(
					'price' => array(
						'field_event_ticket_type' => 'between',
	  				'field_event_ticket_price' => '',
	  				'field_event_ticket_price_from' => preg_match( '/TheatrePerformance/i', $show['EventType']) ? '10,00' : '4,40',
	  				'field_event_ticket_price_to' => preg_match( '/TheatrePerformance/i', $show['EventType']) ? '15,00' : '7,60',
					),
					'availability' => array(
						'field_event_ticket_availability' => 'link',
						'field_event_ticket_link' => $show['EventURL'],
						'field_event_ticket_other' => '',
					),
					'additional' => array(
						'et' => 'https://www.forumcinemas.ee/Tickets/TicketPrices_Ekraan/',
						'ru' => '',
						'en' => '',
					),
				),
	  	);
	  	$excisting_gallery = (isset($existing_api_events[$show['ID']]) && !empty($existing_api_events[$show['ID']]->field_gallery)) ? $existing_api_events[$show['ID']]->field_gallery[LANGUAGE_NONE] : array();
	  	
	  	$gallery = get_event_images($show['Gallery'], $excisting_gallery);
	  	
	  	$step['step_4'] = array(
	  		'gallery' => $gallery
	  	);
	  	$step['step_5'] = array(
	  		'left' => array(
					'field_organizer_name' => 'kino EKRAAN',
					'field_organizer_www' => 'https://www.forumcinemas.ee/',
					'field_organizer_address' => 'Riia 14, 51010 Tartu',
				),
				'right' => array(
					'field_organizer_contact' => 'EKRAAN',
					'field_organizer_email' => 'info@forumcinemas.ee',
					'field_organizer_phone' => '+372 7 343 380',
	 			),
	  	);
	  	$step['node_type'] = 'event';
	  	$step['field_api_provider'] = 'kino_ekraan';
	  	$step['field_api_id'] = $show['ID'];
	  	if(isset($existing_api_events[$show['ID']]) && $existing_api_events[$show['ID']]){
	  		$step['existing_node'] = $existing_api_events[$show['ID']];
	  	}
	  	
	  	$full_event_info[] = $step;
	}
  
	//kpr($full_event_info);
	return $full_event_info;
	
}

function parse_event_date_location($info){
	$event_dates = array();
	//kpr($info);
	if(!empty($info['Shows'])){
		if(key($info['Shows']['Show']) === 0){
			foreach ($info['Shows']['Show'] as $value) {
				$ts = strtotime($value['dttmShowStart']);
				$te = strtotime($value['dttmShowEnd']);
				
				list($start_date, $start_time) = explode(" ", date("d.m.Y H:i", $ts));
				list($end_date, $end_time) = explode(" ", date("d.m.Y H:i", $te));
				
				$event_dates[] = array(
					'date' => array(
						'start' => array(
							'field_event_date_from' => $start_date,
							'field_event_time_from' => $start_time,
						),
						'end' => array(
							'field_event_date_to' => $end_date,
							'field_event_time_to' => $end_time,
						),
					),
					'location' => array(
						'field_event_location' => 'Kino EKRAAN',
						'field_event_address' => 'Riia 14, Tartu, Eesti',
					),
				);
			}
		}else{
			$ts = strtotime($info['Shows']['Show']['dttmShowStart']);
			$te = strtotime($info['Shows']['Show']['dttmShowEnd']);
			
			list($start_date, $start_time) = explode(" ", date("d.m.Y H:i", $ts));
			list($end_date, $end_time) = explode(" ", date("d.m.Y H:i", $te));
			
			$event_dates[] = array(
				'date' => array(
					'start' => array(
						'field_event_date_from' => $start_date,
						'field_event_time_from' => $start_time,
					),
					'end' => array(
						'field_event_date_to' => $end_date,
						'field_event_time_to' => $end_time,
					),
				),
				'location' => array(
					'field_event_location' => 'Kino EKRAAN',
					'field_event_address' => 'Riia 14, Tartu, Eesti',
				),
			);
		}
	}
	
	return $event_dates;
}

function get_event_images($gallery, $excisting_gallery = array()){
	//kpr($gallery);
	//kpr($excisting_gallery);
	$ex_imgs = array();
	$images_array = array();
	$first = TRUE;
	
	if(!empty($excisting_gallery)){
		foreach($excisting_gallery as $imgs){
			$ex_imgs[] = $imgs['field_source_url'][LANGUAGE_NONE][0]['url'];
		}
	}
	if(!empty($gallery)){
		if(key($gallery['GalleryImage']) === 0){
			for ($i = 0; $i < 3; $i++) {
				if(!isset($gallery['GalleryImage'][$i])) break;
				if(!in_array($gallery['GalleryImage'][$i]['Location'], $ex_imgs)){
					//kpr($gallery['GalleryImage'][$i]);
					if($img = ka_download_image_link($gallery['GalleryImage'][$i]['Location'])) $images_array['image_preview']['images'][$img->fid] = array('file' => $img);
				}else{
					foreach($excisting_gallery as $excisting_image){
						$images_array['image_preview']['images'][$excisting_image['fid']] = $excisting_image;
					}
				}
			}
		}else{
			if(isset($gallery['GalleryImage']['Location'])) {
				$gallery_location = $gallery['GalleryImage']['Location'];
			} else if(isset($gallery['Location'])) {
				$gallery_location = $gallery['Location'];
			}
			if(!in_array($gallery_location, $ex_imgs)){
				
				if($img = ka_download_image_link($gallery_location)) $images_array['image_preview']['images'][$img->fid] = array('file' => $img);
			}elseif(!empty($existing_image)){
				$images_array['image_preview']['images'][$excisting_image[0]['fid']] = $excisting_image;
			}
		}
		if(isset($images_array['image_preview']['images']) && !empty($images_array['image_preview']['images'])){
			$images_array['image_preview']['main_image'] = (string) key($images_array['image_preview']['images']);
		}
	}
	
	return $images_array;
}