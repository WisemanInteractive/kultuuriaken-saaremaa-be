<?php

function ka_event_edit_form($form, &$form_state, $nid = NULL) {
  global $user;
  
  if(!empty($nid)) {
    $form['#item_nid'] = $nid;
    $node = node_load($nid);
  }
  
  if(!empty($node)) {
    // Only node owner has access to this menu
    $form['#access'] = $node->uid === $user->uid;
    // Or the account is shared
    if(module_exists('ka_user')) {
      $shared_accounts = ka_user_get_shared_accounts_list();
      
      foreach($shared_accounts as $s) {
        if($node->uid === $s) $form['#access'] = TRUE;
      }
    }
  }
  
  $form['duplicate_event'] = array(
    '#value' => ka_t('Add same'),
    '#type' => 'submit',
    '#action' => 'duplicate_event',
    '#attributes' => array(
      'data-row-action' => 'duplicate_event',
    ),
  );
  
  if($node->type !== 'activity') {
    $form['add_subevent'] = array(
      '#value' => ka_t('Add sub-event'),
      '#type' => 'submit',
      '#action' => 'add_subevent',
      '#attributes' => array(
        'data-row-action' => 'add_subevent',
      ),
    );
  }
  
  $form['change_event'] = array(
    '#value' => ka_t('Change'),
    '#type' => 'submit',
    '#action' => 'change_event',
    '#attributes' => array(
      'data-row-action' => 'change_event',
    ),
  );
  
  $form['delete_event'] = array(
    '#value' => ka_t('Delete'),
    '#type' => 'submit',
    '#action' => 'delete_event',
    '#attributes' => array(
      'data-row-action' => 'delete_event',
    ),
  );
  
  return $form;
}

function ka_event_edit_form_validate($form, &$form_state) {
  
}

function ka_event_edit_form_submit($form, &$form_state) {
  global $user;

  $item_nid = $form['#item_nid'];
  $triggering_element = $form_state['triggering_element']['#action'];

  switch($triggering_element) {
    case 'duplicate_event':
      $entity_array = entity_load('node', array($item_nid));
      $entity = array_shift($entity_array);
      $entity_type = $entity->type;
      $clone = replicate_clone_entity('node', $entity);
      
      $emw_clone = entity_metadata_wrapper('node', $clone);
      
      $current_ka_status = $emw_clone->field_ka_status->value();

      // Switch the clone status to unpublished unless it is unfinished.
      if($current_ka_status !== 'unfinished') {
        $emw_clone->field_ka_status = 'unpublished';
      }
      
     
      // Set title to each enabled language
      $language_list = language_list();
      foreach($language_list as $lang => $language) {
        $set_language = $emw_clone->language($lang)->value();
        // Check if the language is set or default is loaded instead.
        if($lang === $set_language->language) {
          // Add 
          $title = $emw_clone->language($lang)->label();
          $emw_clone->language($lang)->title = sprintf('%s (%s)', $title, ka_t('Copy'));
        }
      }
      
      drupal_set_message(ka_t('@title duplicated!', array('@title' => $emw_clone->label())));
      $emw_clone->save();
      break;
    case 'add_subevent':
      $node = node_load($item_nid);
      if($node) {
        drupal_goto('add/' . $node->type, array('query' => array('parent_id' => $item_nid)));
      }
      break;
    case 'delete_event':
      $node = node_load($item_nid);
      if(isset($node->nid)) {
        node_delete($node->nid);
        
        drupal_set_message(ka_t('Successfully deleted!'));
      }
      break;
    case 'change_event':
      $node = node_load($item_nid);
      if($node) {
        $form_state['redirect'] = 'edit/' . $item_nid . '/' . $node->type;
      }
  }
}

function ka_event_form_alter(&$form, &$form_state, $form_id) {
  // https://www.drupal.org/node/2185239 workaround #6
  
  if(strpos($form_id, 'ka_event_edit_form') === 0) {
    $query = drupal_http_build_query(drupal_get_query_parameters());
    $form['#action'] = base_path() . drupal_get_path_alias() . '?' . $query;
  }

}