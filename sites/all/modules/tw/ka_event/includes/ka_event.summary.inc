<?php

/* FROM SUMMARY ELEMENTS */

function introduction_summary(&$form, $form_state, $step) {
  $form['summary'][$step] = array(
      '#markup' => theme('introduction_summary', array('data' => $form_state['storage']["step_$step"], 'node_type' => $form['#node_type'])),
    );
    
  $form['summary']["change_$step"] = array(
    '#type' => 'submit',
    '#limit_validation_errors' => array(),
    '#value' => $step,
    '#submit' => array('ka_change_step'),
    '#name' => 'change',
    '#ajax' => array(
      'callback' => 'next_step_callback',
      'wrapper' => 'add-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'data-id' => array('change-introduction-summary'),
      'style' => array('display:none;'),
    ),
  );
}

function category_summary(&$form, $form_state, $step) {
  $form['summary'][$step] = array(
      '#markup' => theme('category_summary', array('data' => $form_state['storage']["step_$step"], 'node_type' => $form['#node_type'])),
    );
    
    $form['summary']["change_$step"] = array(
      '#type' => 'submit',
      '#limit_validation_errors' => array(),
      '#value' => $step,
      '#submit' => array('ka_change_step'),
      '#name' => 'change',
      '#ajax' => array(
        'callback' => 'next_step_callback',
        'wrapper' => 'add-form',
        'method' => 'replace',
    ),
      '#attributes' => array(
        'data-id' => array('change-category-summary'),
        'style' => array('display:none;'),
    ),
  );
}

function event_ticket_summary(&$form, $form_state, $step) {
  $form['summary'][$step] = array(
      '#markup' => theme('ticket_summary', array('data' => $form_state['storage']["step_$step"])),
    );
    
    $form['summary']["change_$step"] = array(
      '#type' => 'submit',
      '#limit_validation_errors' => array(),
      '#value' => $step,
      '#submit' => array('ka_change_step'),
      '#name' => 'change',
      '#ajax' => array(
        'callback' => 'next_step_callback',
        'wrapper' => 'add-form',
        'method' => 'replace',
      ),
      '#attributes' => array(
        'data-id' => array('change-ticket-summary'),
        'style' => array('display:none;'),
      ),
  );
}

function gallery_summary(&$form, $form_state, $step) {
  $form['summary'][$step] = array(
      '#markup' => theme('gallery_summary', array('data' => $form_state['storage']["step_$step"])),
    );

  $form['summary']["change_$step"] = array(
    '#type' => 'submit',
    '#limit_validation_errors' => array(),
    '#value' => $step,
    '#submit' => array('ka_change_step'),
    '#name' => 'change',
    '#ajax' => array(
      'callback' => 'next_step_callback',
      'wrapper' => 'add-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'data-id' => array('change-gallery-summary'),
      'style' => array('display:none;'),
    ),
  );
}

function organizer_summary(&$form, $form_state, $step) {
  $form['summary'][$step] = array(
    '#markup' => theme('organizer_summary', array('data' => $form_state['storage']["step_$step"])),
  );
    
  $form['summary']["change_$step"] = array(
    '#type' => 'submit',
    '#limit_validation_errors' => array(),
    '#value' => $step,
    '#submit' => array('ka_change_step'),
    '#name' => 'change',
    '#ajax' => array(
      'callback' => 'next_step_callback',
      'wrapper' => 'add-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'data-id' => array('change-organizer-summary'),
      'style' => array('display:none;'),
    ),
  );
}

function activity_groups_summary(&$form, $form_state, $step) {
  
  $form['summary'][$step] = array(
    '#theme' => 'activity_groups_summary',
    '#type' => 'container',
    '#data' => $form_state['storage']["step_$step"],
    '#prefix' => '<div id="groups-summary">',
    '#suffix' => '</div>',
  );
  
  foreach($form_state['storage']['step_3']['groups'] as $i => $group) {
    $form['summary'][$step]['is_full'][$i] = array(
      '#theme_wrappers' => array(),
      '#type' => 'checkbox',
      '#group_id' => $i,
      '#title' => ka_t('Group is full'),
      '#submit' => array('set_is_full'),
      '#validate' => array('set_is_full'),
      '#limit_validation_errors' => array(),
      '#default_value' => !empty($form_state['storage']['step_3']['groups'][$i]['is_full']) ? 
        $form_state['storage']['step_3']['groups'][$i]['is_full'] : 0,
      '#ajax' => array(
        'callback' => 'is_full_callback',
        'wrapper' => 'groups-summary',
        'method' => 'replace',
      ),
    );

    if($form['summary'][$step]['is_full'][$i]['#default_value'] === 1) {
      $form['summary'][$step]['is_full'][$i]['#attributes']['checked'] = 'checked';
    }
  }
  
  $form['summary']["change_$step"] = array(
    '#type' => 'submit',
    '#limit_validation_errors' => array(),
    '#value' => $step,
    '#submit' => array('ka_change_step'),
    '#name' => 'change',
    '#ajax' => array(
      'callback' => 'next_step_callback',
      'wrapper' => 'add-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'data-id' => array('change-activity-group-summary'),
      'style' => array('display:none;'),
    ),
  );
}