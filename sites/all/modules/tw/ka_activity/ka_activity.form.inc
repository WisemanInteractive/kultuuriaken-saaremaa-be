<?php

function ka_activity_form_activity_node_form_alter(&$form, &$form_state, $form_id) {
  
  $form['#tree'] = TRUE;
  
  $form['field_ka_status']['#access'] = FALSE;
  $form['language']['#access'] = FALSE;
  $form['options']['#access'] = FALSE;
  $form['author']['#access'] = FALSE;
  $form['revision_information']['#access'] = FALSE;
  $form['additional_settings']['#access'] = FALSE;
  $form['#theme'][] = 'ka_activity_form';
  
  // Tutvustus
  //$form['step_1']['title_field'] = $form['title_field'];
  //unset($form['title_field']);
  
  //krumo($form);
  
}