<?php 
class ka_organizer_handler_filter_period extends views_handler_filter {

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    if (!empty($form_state['exposed'])) {
      $form['value'] = array(
        '#theme_wrappers' => array(),
        '#type' => 'textfield',
        '#title' => t('Event period'),
        '#title_display' => FALSE,
        '#default_value' => NULL,
        '#attributes' => array(
            'data-plugin' => 'datepickerRange',
          ),
      );
    }
  }
  
  function query() {
    $this->ensure_my_table();
    
    if(!empty($this->value[0])) {
      $date_range = trim($this->value[0]);
      if(preg_match('/\d{2}\.\d{2}.\d{4}\s\-\s\d{2}\.\d{2}.\d{4}/', $date_range) === 1) {
        $dates = explode(' - ', $date_range);
        $from_date = strtotime($dates[0]);
        $to_date = strtotime($dates[1]);

        $join = new views_join();
        
        $join->table = 'field_data_field_event_date';
        $join->left_table = 'paragraphs_item_field_data_field_event_dates';
        
        $join->field = 'entity_id';
        $join->left_field = 'item_id';
        $join->type = 'INNER';
        
        
        $this->query->table_queue['paragraphs_item_field_data_field_event_dates__field_data_field_event_date'] = array (
          'alias' => 'paragraphs_item_field_data_field_event_dates__field_data_field_event_date',// I believe this is optional
          'table' => 'field_data_field_event_date',
          'relationship' => 'field_data_field_event_date',
          'join' => $join,
        );
        
        $this->query->where[] = array(
          'conditions' => array(
            array(
              'field' => 'paragraphs_item_field_data_field_event_dates__field_data_field_event_date.field_event_date_value',
              'value' => $from_date,
              'operator' => '>=',
            ),
            array(
              'field' => 'paragraphs_item_field_data_field_event_dates__field_data_field_event_date.field_event_date_value2',
              'value' => $to_date,
              'operator' => '<=',
            ),
            // add more conditions if you want to
          ),
          'type' => 'AND',
          //'type' => 'OR' ,// I believe this is an operator for multiple conditions
        );
        
      }
    }
  }
}