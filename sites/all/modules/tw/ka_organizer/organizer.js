(function ($) {
  Drupal.behaviors.organizer = {
    attach: function (context, settings) {
      
      $(document).find('a.show-all, a.show-less').click(function(e) {
        e.preventDefault();
        if($(this, context).hasClass('show-all')) {
          $(this, context).parents('.view-organizer-tables').find('select[name="items_per_page"]').val('All');
        } else if($(this, context).hasClass('show-less')) {
          $(this, context).parents('.view-organizer-tables').find('select[name="items_per_page"]').val(5);
        }
        $(this, context).parents('.view-organizer-tables').find('button[type="submit"]#edit-submit-organizer-tables').trigger('click');
      });
      
      $(document, context).find('.view-organizer-tables form').find('select, input').change(function() {

      });
      
      $(document).ajaxComplete(function() {
        if(typeof $.fn.SumoSelect !== 'undefined') {
          $('[data-plugin="SumoSelect"]').SumoSelect();
        }
      });
      
      // User page functions
      $(document, context).find('#edit-account-enable').click(function(e) {
        e.preventDefault();
        $(this, context).siblings('button[type="submit"]').trigger('mousedown');
      });
      
      function initOrganizerMenu() {
        $(document, context).on( 'click', '.ka-add-same', function(e) {
          e.preventDefault();
          
          var rowId = $(this).attr('data-row');
          
          $(document, context).find('[data-row-form="' + rowId + '"]').find('button[data-row-action="duplicate_event"]').click();
        });
        
        $(document, context).on( 'click', '.ka-add-subevent', function(e) {
          e.preventDefault();
          
          var rowId = $(this).attr('data-row');
          $(document, context).find('[data-row-form="' + rowId + '"]').find('button[data-row-action="add_subevent"]').click();
        });
        
        $(document, context).on( 'click', '.ka-delete', function(e) {
          e.preventDefault();
          var rowId = $(this).attr('data-row');
          $(document, context).find('[data-row-form="' + rowId + '"]').find('button[data-row-action="delete_event"]').click();
        });
        
        $(document, context).on( 'click', '.ka-change', function(e) {
          e.preventDefault();
          
          var rowId = $(this).attr('data-row');
          $(document, context).find('[data-row-form="' + rowId + '"]').find('button[data-row-action="change_event"]').click();
        });
      }
      
      initOrganizerMenu();
      initSort();
      $(document, context).ajaxStop(function() {
        initOrganizerMenu();
        initSort();
      });
      
      // Sort functionality
      function initSort() {
        $(document, context).find('.view-organizer-tables').each(function() {
          var sortOrder = $(this).find('[name="sort_order"]');
          var sortClass = 'before-arrow_' + (sortOrder.val() === 'ASC' ? 'up' : 'down');
        
          var dataOrganiser = $('[data-organiser-sort]', this);
          $('i', dataOrganiser).attr('class', sortClass);
        
          var submitButton = $('.views-submit-button', this).find('button[type="submit"]');
          $(dataOrganiser).click(function()
            {
              if(sortOrder.val() === 'ASC') {
                sortOrder.val('DESC');
              } else {
                sortOrder.val('ASC');
              }
              $(submitButton).click();
            } 
          );
        });
      }
    }
}
})(jQuery);