<?php

// Account delete form
function ka_organizer_delete_form($form, &$form_state, $uid = NULL) {
  global $user;
  $d_user = user_load($uid);
  
  $needs_confirmation = isset($form_state['needs_confirmation']) ? $form_state['needs_confirmation'] : FALSE; 
  
  $form['#prefix'] = '<div id="delete-account-container">';
  $form['#suffix'] = '</div>';
  
  if($user->uid !== $d_user->uid) {
    $form['#access'] = FALSE;
  }
  
  if(!empty($uid) 
  && !in_array('administrator', $d_user->roles) 
  && !$needs_confirmation
  ) {
    $form['delete_user'] = array(
      '#type' => 'submit',
      '#submit' => array('delete_confirm'),
      '#value' => ka_t('Delete my account'),
      '#attributes' => array(
        'class' => array('btn-filled'),
      ),
      '#ajax' => array(
        'callback' => 'delete_callback',
        'wrapper' => 'delete-account-container',
      ),
    );
  }
  
  $form['delete_uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );
  
  if($needs_confirmation) {
    return confirm_form
    (
      $form,
      ka_t('<h6>Are you sure?</h6>'),
      'user'
    );
  }
  
  return $form;
}

function delete_confirm($form, &$form_state) {
  $form_state['needs_confirmation'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

function delete_callback($form, &$form_state) {
  return $form;
}

function ka_organizer_delete_form_validate($form, &$form_state) {
  
}

function ka_organizer_delete_form_submit($form, &$form_state) {
  drupal_set_message(ka_t('You account has been deleted!'));
  $uid = $form_state['values']['delete_uid'];
  
  $form_state['redirect'] = '<front>';
  user_cancel(array(), $uid, 'user_cancel_block_unpublish');
}

// Account edit form
function ka_organizer_edit_form($form, &$form_state, $uid) {
  global $user;
  $edit_user = user_load($uid);
  $emw_user = entity_metadata_wrapper('user', $edit_user);
  
  $form['#prefix'] = '<div id="user-data">';
  $form['#suffix'] = '</div>';
  
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'ka_organizer') . '/organizer.js',
  );
  
  if(!isset($form_state['edit_enabled']) && !empty($user->mail)) {
    $form_state['edit_enabled'] = FALSE; // Off by default
  } else {
    $form_state['edit_enabled'] = TRUE; // Enabled if the e-mail is not set.
  }
  
  if(isset($form_state['edit_enabled']) && $form_state['edit_enabled']) {
    $form['name'] = array(
      '#type' => 'textfield', 
      '#title' => ka_t('Name'),
      '#title_display' => 'invisible',
      '#default_value' => $emw_user->field_full_name->value(), 
      '#size' => 60, 
      '#maxlength' => 128, 
      '#required' => TRUE,
    );
    
    $form['institution'] = array(
      '#type' => 'textfield', 
      '#title' => ka_t('Institution'),
      '#title_display' => 'invisible',
      '#default_value' => $emw_user->field_institution->value(), 
      '#size' => 60, 
      '#maxlength' => 128, 
      '#required' => FALSE,
    );
    
    $form['mail'] = array(
      '#type' => 'textfield', 
      '#title' => ka_t('E-Mail'),
      '#title_display' => 'invisible',
      '#default_value' => $emw_user->mail->value(), 
      '#size' => 60, 
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    
    $form['phone'] = array(
      '#type' => 'textfield', 
      '#title' => ka_t('Phone'),
      '#title_display' => 'invisible',
      '#default_value' => $emw_user->field_user_phone->value(), 
      '#size' => 60, 
      '#maxlength' => 128, 
      '#required' => FALSE,
    );
    
    $form['save_changes'] = array(
      '#type' => 'submit', 
      '#value' => ka_t('Save'),
      '#ajax' => array(
        'callback' => 'edit_callback',
        'wrapper' => 'user-data',
      ),
    );
  } else if($user->uid === $edit_user->uid) {
    $form['change'] = array(
      '#type' => 'submit',
      '#submit' => array('toggle_editing'),
      '#ajax' => array(
        'callback' => 'edit_callback',
        'wrapper' => 'user-data',
      ),
      '#attributes' => array(
        'style' => 'display:none;',
      ),
      '#value' => ka_t('Change'),
      '#limit_validation_errors' => array(),
    );
  }
  
  $form['edit_uid'] = array(
    '#type' => 'hidden', 
    '#value' => $edit_user->uid,
  );

  return $form;
}

function ka_organizer_edit_form_validate($form, &$form_state) {
  $mail = isset($form_state['values']['mail']) ? $form_state['values']['mail'] : NULL;
  if(!valid_email_address($mail)) {
    form_set_error('mail', ka_t('Invalid e-mail address!'));
  }
  /*
  if(user_load_by_mail($mail) && $mail <> $form['mail']['#default_value']) { // Check if such account already exists
    form_set_error('mail', ka_t('An account with the e-mail already exists!'));
  }
  */
}

function ka_organizer_edit_form_submit($form, &$form_state) {
  $uid = $form_state['values']['edit_uid'];
  $user = user_load($uid);
  $emw_user = entity_metadata_wrapper('user', $user);
  
  $emw_user->field_full_name = $form_state['values']['name'];
  $emw_user->field_institution = $form_state['values']['institution'];
  $emw_user->field_user_phone = $form_state['values']['phone'];
  $emw_user->mail = $form_state['values']['mail'];
  
  $emw_user->save();
  
  toggle_editing($form, $form_state);
  
  drupal_set_message(ka_t('User account successfully updated!'));
}

function toggle_editing($form, &$form_state) {
  $form_state['edit_enabled'] = $form_state['edit_enabled'] === FALSE ? TRUE : FALSE;
  $form_state['rebuild'] = TRUE;
}

function edit_callback($form, &$form_state) {
  return $form;
}