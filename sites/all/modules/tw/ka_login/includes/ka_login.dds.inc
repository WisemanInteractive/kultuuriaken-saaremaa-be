<?php

require_once DRUPAL_ROOT . '/vendor/autoload.php';

define('CHOOSE_LOGIN_METHOD', 1);
define('ENTER_LOGIN_CREDENTIALS', 2);
define('AUTHENTICATE_USER', 3);

function ka_login_form($form, &$form_state) {
    drupal_page_is_cacheable(FALSE);
    $form['#theme'] = 'ka_login_form_template';
    $form_state['login_step'] = !empty($form_state['login_step'])
        ? $form_state['login_step'] : CHOOSE_LOGIN_METHOD;
    $login_step = $form_state['login_step'];
    $login_method = !empty($form_state['login_method'])
        ? $form_state['login_method'] : NULL;

    $form['#attached']['js'][] = array(
        'data' => drupal_get_path('module', 'ka_login') . '/includes/ka-login.js',
        'type' => 'file',
    );

    $separator = '';
    $login_prefix = '';

    switch($form_state['login_step']) {
        case CHOOSE_LOGIN_METHOD:
            $header_text = ka_t('Log in securely') . ':';
            $separator = ' separator';
            break;
        case ENTER_LOGIN_CREDENTIALS:
            $header_text = ka_t('Enter login credentials') . ':';
            if($login_method !== 'mobile_id') {
                $login_prefix = '<div class="col-3"></div>';
            }

            break;
        case AUTHENTICATE_USER:
            $header_text = ka_t('Authenticate user through your device') . ':';
            break;
    }



    $form['login_container'] = array(
      '#type' => 'container',
      '#prefix' => '<div class="block rounded" id="login-container"><h3>' . $header_text . '</h3><div class="row'.$separator.'">',
      '#suffix' => '</div></div>',
        '#theme_wrappers' => array(),
    );

    $col_width = 12;
    $mobile_id_enabled = variable_get('ka_login_mobile_id_enabled', FALSE);
    $smart_id_enabled = variable_get('ka_login_smart_id_enabled', FALSE);

    if($mobile_id_enabled) {
        $col_width -= 6;
    }
    if($smart_id_enabled) {
        $col_width -= 2;
    }

    // Step 1: Choose login method
    $form['login_container']['id_card'] = array(
        '#type' => 'image_button',
        '#src' => drupal_get_path('theme', 'kultuuriaken') . '/assets/imgs/id-kaart.svg',
        '#prefix' => '<div class="col-'.$col_width.'"><div class="login-buttons no-top-margin">',
        '#suffix' => ka_t('Log in with ID Card') . '</div></div>',
        '#access' => $login_step === CHOOSE_LOGIN_METHOD,
        '#ajax' => array(
            'callback' => 'ka_login_set_id_auth_method',
            'wrapper' => 'ka-login-form',
        ),
    );

    $form['login_container']['mobile_id'] = array(
        '#type' => 'image_button',
        '#src' => drupal_get_path('theme', 'kultuuriaken') . '/assets/imgs/mobiil-id.svg',
        '#ajax' => array(
            'callback' => 'ka_login_set_auth_method',
            'wrapper' => 'login-container',
        ),
        '#prefix' => '<div class="col-'.$col_width.'"><div class="login-buttons no-top-margin">',
        '#suffix' => ka_t('Log in with Mobile ID') . '</div></div>',
        '#access' => $login_step === CHOOSE_LOGIN_METHOD && variable_get('ka_login_mobile_id_enabled', FALSE),
    );

    $form['login_container']['smart_id'] = array(
        '#type' => 'image_button',
        '#src' => drupal_get_path('theme', 'kultuuriaken') . '/assets/imgs/mobiil-id.svg',
        '#ajax' => array(
            'callback' => 'ka_login_set_auth_method',
            'wrapper' => 'login-container',
        ),

        '#prefix' => '<div class="col-'.$col_width.'"><div class="login-buttons no-top-margin">',
        '#suffix' => ka_t('Log in with Smart ID') . '</div></div>',
        '#access' => $login_step === CHOOSE_LOGIN_METHOD && variable_get('ka_login_smart_id_enabled', FALSE),
    );

    // Step 2: Enter credentials
    $form['login_container']['id_code'] = array(
        '#type' => 'textfield',
        '#title' => ka_t('ID Code'),
        '#title_display' => 'invisible',
        '#prefix' => $login_prefix . '<div class="col-5"><div class="login-buttons no-top-margin">' . ka_t('ID Code'),
        '#suffix' => '</div></div>',
        '#access' => $login_step === ENTER_LOGIN_CREDENTIALS,
        '#required' => TRUE,
    );

    $form['login_container']['phone_number'] = array(
        '#type' => 'textfield',
        '#title' => ka_t('Phone number'),
        '#title_display' => 'invisible',
        '#prefix' => '<div class="col-5"><div class="login-buttons no-top-margin">' . ka_t('Phone number'),
        '#suffix' => '</div></div>',
        '#access' => $login_step === ENTER_LOGIN_CREDENTIALS && $login_method === 'mobile_id',
        '#required' => TRUE,
        '#attributes' => array(
            'placeholder' => '+372XXXXXXXX',
        ),
    );

    $form['login_container']['submit'] = array(
        '#type' => 'submit',
        '#prefix' => '<div class="col-2"><div class="login-buttons no-top-margin">',
        '#suffix' => '</div></div>',
        '#access' => $form_state['login_step'] !== CHOOSE_LOGIN_METHOD,
        '#value' => ka_t('Submit'),
        '#ajax' => array(
            'callback' => 'ka_login_set_auth_method',
            'wrapper' => 'login-container',
        ),
    );

    // Step 3: Authenticate user
    $form['login_container']['challenge_id'] = array(
      '#markup' => '<div class="col-12"><div class="challenge-id">' . $form_state['challenge_id'] . '</div></div>',
      '#access' => $login_step === AUTHENTICATE_USER,
    );

    if($login_step === ENTER_LOGIN_CREDENTIALS && $login_method === 'id_card') {

    }

    if($login_step === AUTHENTICATE_USER) {
        // Add verification JS
        if($login_method === 'id_card') {
            drupal_add_js(array('ka_login' => array('idCode' => $form_state['values']['id_code'])), 'setting');
        }

        // Hide login button
        $form['login_container']['submit']['#attributes'] = array('style' => 'display:none;');
    }

    return $form;
}

function ka_login_form_validate($form, &$form_state) {
    /*
    $login_step = $form_state['login_step'];

    if($login_step === ENTER_LOGIN_CREDENTIALS) {
      $login_method = $form_state['login_method'];
      // Validate ID Code and Phone number
      $id_code = $form_state['values']['id_code'];

      $entered_phone_number = $form_state['values']['phone_number'];

      if($user = user_load_by_id_code($id_code)) {

          // User exists, validate the phone number in case of Mobile ID
          if($login_method === 'mobile_id') {
              if(!empty($user->field_user_phone)) {
                  $phone_number = $user->field_user_phone[LANGUAGE_NONE][0]['value'];

                  if($phone_number != $entered_phone_number) {
                      // Phone number does not match
                      form_set_error('phone_number', ka_t('Invalid Phone Number'));
                  }
              } else {
                  // Phone number not set
                  form_set_error('phone_number', ka_t('Invalid Phone Number'));
              }
          }
      } else {
          // No user with such ID code
          form_set_error('id_code', ka_t('Invalid ID Code'));
      }
    }
    */
}

function ka_login_form_submit(&$form, &$form_state) {
    $form_state['rebuild'] = TRUE;

    $login_step = $form_state['login_step'];

    switch($login_step) {
        case CHOOSE_LOGIN_METHOD:
            $form_state['login_step'] = ENTER_LOGIN_CREDENTIALS;
            $form_state['login_method'] = $form_state['triggering_element']['#name'];
            break;
        case ENTER_LOGIN_CREDENTIALS:
            $id_code = $form_state['values']['id_code'];

            $login_method = $form_state['login_method'];

            switch($login_method) {
                case 'mobile_id':
                    $phone_number = $form_state['values']['phone_number'];

                    // Send DDS authentication request
                    if($challenge_id = ka_login_dds_service($phone_number, $id_code)) {
                        $form_state['challenge_id'] = $challenge_id;
                    };

                    break;
                case 'smart_id':
                    $auth = ka_login_smart_id_service($id_code);
                    $_SESSION['smart_id_auth'] = serialize($auth['auth']);
                    $form_state['challenge_id'] = $auth['verificationCode'];
                    break;
                case 'id_card':
                    $_SESSION['id_code'] = $id_code;
                    drupal_add_js(array('ka_login' => array('idCard' => TRUE), 'setting'));
                    break;
            }
            $form_state['login_step'] = AUTHENTICATE_USER;
            break;
        case AUTHENTICATE_USER:
            drupal_goto('dds/verify');
            break;
    }
}

function ka_login_form_alter(&$form, &$form_state, $form_id) {
    if($form_id === 'ka_login_form') {
        // Forward to auth URL.
        if($form_state['triggering_element']['#name'] === 'id_card') {
            $form['#action'] = '/auth/';
            unset($form['login_container']['submit']['#ajax']);
        }
    }
}

function ka_login_set_auth_method($form, $form_state) {
    return $form['login_container'];
}

function ka_login_set_id_auth_method($form, $form_state) {
    return $form;
}

// Mobile ID
function ka_login_dds_service($phone_number, $id_code) {
    try {
        $wsdl = variable_get('ka_login_dds_wsdl', 'https://tsp.demo.sk.ee/dds.wsdl');

        $ip = $_SERVER['SERVER_ADDR'] . ':0';
        $options = array('socket' => array('bindto' => $ip));
        $context = stream_context_create($options);

        $dds = new SoapClient($wsdl, array('stream_context' => $context));

        $params = variable_get('ka_login_dds_params',
            array(
            'IDCode' => NULL,
            'CountryCode' => 'EE',
            'PhoneNo' => NULL,
            'Language' => 'EST',
            'ServiceName' => 'Testimine',
            'MessageToDisplay' => 'Saaremaa Sündmused',
            'SPChallenge' => '',
            'MessagingMode' => 'asynchClientServer',
            'ReturnCertData' => TRUE,
            'ReturnRevocationData' => TRUE,
            )
        );


        $params['PhoneNo'] = $phone_number;
        $params['IDCode'] = $id_code;

        $result = $dds->__soapCall('MobileAuthenticate', $params);

        if($result['Status'] === 'OK') {
            $_SESSION['Sesscode'] = $result['Sesscode'];
            $_SESSION['id_code'] = $id_code;

            return $result['ChallengeID'];
        }
    } catch (SoapFault $e) {
        dpm($e);
    }
    return FALSE;
}

function ka_login_dds_service_verify()
{
    drupal_page_is_cacheable(FALSE);
    if(!empty($_GET['i'])) { // ID Card login
        $cache = cache_get($_GET['i']);
        if(time() < ($cache->expire + $cache->created)) {
            $_SESSION['id_code'] = $cache->data;
            $_SESSION['dds_verified'] = TRUE;
        }
    }

    // Log user in
    if(empty($_GET['check']) && !empty($_SESSION['dds_verified']) && !empty($_SESSION['id_code'])) {
        unset($_SESSION['dds_verified']);
        $id_code = $_SESSION['id_code'];
        /*$user_info = user_external_load($id_code);
        if(!empty($user_info)) { // Log in existing user.
            global $user;
            $user = $user_info;
            user_login_finalize();
            drupal_goto('<front>');
        } else {
            // Create new user
            $id_code = $_SESSION['id_code'];
            ka_login_create_new_user_id($id_code);
        }*/
        ka_login_create_new_user_id($id_code);
    }

    // No pending request
    if ((empty($_SESSION['Sesscode']) && empty($_SESSION['smart_id_auth']))
        || empty($_SESSION['id_code'])
        || user_is_logged_in()) {
        drupal_json_output(array('status' => 'PENDING'));
        return;
    }

    if (!empty($_GET['check'])) {
        if(!empty($_SESSION['Sesscode'])) {
            $result = ka_login_dds_service_authenticate();
        }
        if(!empty($_SESSION['smart_id_auth'])) {
            $result = ka_login_smart_id_service_authenticate();
        }
        drupal_json_output(array('status' => $result['Status']));
    }
}

function ka_login_dds_service_authenticate() {
    $sess_code = $_SESSION['Sesscode'];
    unset($_SESSION['Sesscode']);

    $wsdl = variable_get('ka_login_dds_wsdl', 'https://tsp.demo.sk.ee/dds.wsdl');

    $ip = $_SERVER['SERVER_ADDR'] . ':0';
    $options = array('socket' => array('bindto' => $ip));
    $context = stream_context_create($options);

    $dds = new SoapClient($wsdl, array('stream_context' => $context));

    $params = array(
        'Sesscode' => $sess_code,
        'WaitSignature' => TRUE,
    );

    // Verify login
    try {
        $result = $dds->__soapCall('GetMobileAuthenticateStatus', $params);
        if ($result['Status'] === 'USER_AUTHENTICATED') {
            $_SESSION['dds_verified'] = TRUE;
        }
        return $result;
    } catch (SoapFault $e) {
        watchdog('ka_login', $e);
        return array('status' => 'INTERNAL_ERROR');
    }
}

// Smart ID
function ka_login_smart_id_service($id_code, $country = 'EE') {
    $client = new \Sk\SmartId\Client();

    $auth_endpoint = variable_get(
        'ka_login_smart_id_endpoint',
        'https://sid.demo.sk.ee/smart-id-rp/v1/'
    );

    $params = variable_get(
        'ka_login_smart_id_params',
        array(
            'relyingPartyUUID' => '00000000-0000-0000-0000-000000000000',
            'RelyingPartyName' => 'DEMO',
            'displayText' => 'Saaremaa Sündmused',
        )
    );

    $client
        ->setRelyingPartyUUID($params['relyingPartyUUID'])
        ->setRelyingPartyName($params['RelyingPartyName'])
        ->setHostUrl($auth_endpoint);

    $identity = new \Sk\SmartId\Api\Data\NationalIdentity($country, $id_code);

    $authenticationHash = \Sk\SmartId\Api\Data\AuthenticationHash::generate();
    $verificationCode = $authenticationHash->calculateVerificationCode();

    $authenticationResponse = $client->authentication()
        ->createAuthentication()
        ->withNationalIdentity( $identity ) // or with document number: ->withDocumentNumber( 'PNOEE-1111111111-XXXX-XX' )
        ->withAuthenticationHash( $authenticationHash )
        ->withCertificateLevel( \Sk\SmartId\Api\Data\CertificateLevelCode::QUALIFIED );

    $_SESSION['id_code'] = $id_code;

    return array('auth' => $authenticationResponse, 'verificationCode' => $verificationCode);
}

function ka_login_smart_id_service_authenticate() {
    if(empty($_SESSION['smart_id_auth'])) return;

    $auth = unserialize($_SESSION['smart_id_auth']);
    unset($_SESSION['smart_id_auth']);

    try{
        $query = $auth->authenticate();
        $result = $query->getEndResult() === 'OK';
        if($result) {
            $_SESSION['dds_verified'] = TRUE;
        }
    } catch (SmartIdException $e) {
        watchdog('ka_login', $e);
        $result = 'INTERNAL_ERROR';
    }

    return $result ? array('Status' => 'USER_AUTHENTICATED')
        : array('Status' => 'INTERNAL_ERROR');
}

function user_load_by_id_code($id_code) {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'user')
        ->propertyCondition('status', TRUE)
        ->fieldCondition('field_id_code', 'value', $id_code);

    $result = $query->execute();

    if(!empty($result['user'])) {
        $uid = reset($result['user'])->uid;
        return user_load($uid);
    } else {
        return FALSE;
    }
}

// Create new user
function ka_login_create_new_user_id($id_code) {
    global $user;

    $role = user_role_load_by_name("organizer");

    user_external_login_register($id_code, 'ka_login');

    if(!user_has_role($role->rid)) { // Add organizer role if it is missing.
        user_multiple_role_edit(array($user->uid), 'add_role', $role->rid);
    }

    if ($user && empty($user->mail)) {
        drupal_set_message(ka_t('Please enter your e-mail address.'));
        drupal_goto('user');
    }   elseif(user_is_logged_in() && !empty($user->mail)) {
        drupal_goto('korraldaja');
    }

}