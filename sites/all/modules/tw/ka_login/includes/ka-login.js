(function ($) {

    console.log('Starting init');
    var initialized;

    function init() {
        if(!initialized) {
            console.log('Running init');
            initialized = true;
            checkStatus();
        }
    }

    function checkStatus() {
        $.getJSON('dds/verify?check=1', function(data) {
            if(data.status === 'USER_AUTHENTICATED') {
                $('form[id="ka-login-form"]').submit();
            } else {
                console.log(data);
                setTimeout(checkStatus, 5000);
            }
        });
    }

    Drupal.behaviors.kaLoginVerify = {
        attach: function(context, settings) {
            init();
        }
    };

}(jQuery));