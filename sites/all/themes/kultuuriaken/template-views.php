<?php
/**
 * @file
 * Organizer page specific template functions.
 */

function kultuuriaken_preprocess_views_view_fields(&$vars) {

  if($vars['view']->current_display === 'recommended'
    || $vars['view']->current_display === 'similar_events'
    || $vars['view']->current_display === 'similar_activities'
    || $vars['view']->current_display === 'similar_events_anon'
    || $vars['view']->current_display === 'similar_activities_anon'
  ) {
    $nid = $vars['row']->nid;
    $node = node_load($nid);
    $node_array = ka_node_to_array($node);
    if(!empty($node_array)) {
      $vars['fields']['summary'] = reset($node_array);
    }
  }
  if($vars['view']->current_display === 'upcoming_activities') { // Algavad huvitegevused
      $nid = $vars['row']->nid;
      $node = node_load($nid);
      $node_array = ka_node_to_array($node);

      $seven_days_from_now = strtotime('now + 7 days');
      $now = time();
      if(!empty($node_array)) {
        foreach($node_array as $n_group) {
          if($n_group['start_date'] > $now && $n_group['start_date'] < $seven_days_from_now) {
            // Check for the closest matching date.
            $vars['fields']['summary'] = $n_group;
            break;
          }
        }
      }
    }
}

function kultuuriaken_preprocess_views_view_table__organizer_tables(&$vars) { 
  $vars['classes_array'][] = 'main';
  $vars['classes_array'][] = 'statuses';
  //$vars['classes_array'][] = 'organizer-events-rows';
  $vars['attributes_array']['data-plugin'] = 'sortTable';

  // Add node edit form for items
  $vars['item_menu'] = array();
  foreach($vars['result'] as $item) {
    $vars['item_menu'][] = drupal_get_form('ka_event_edit_form_' . $item->nid, $item->nid);
  }
}

function kultuuriaken_preprocess_views_view_table__organizer_tables__sub_events(&$vars) {
  $vars['item_menu'] = array();
  foreach($vars['result'] as $item) {
    $vars['item_menu'][] = drupal_get_form('ka_event_edit_form_' . $item->nid, $item->nid);
  }
}

function kultuuriaken_preprocess_views_view__organizer_tables(&$vars) {
  // Show all/less button
  $total_rows = $vars['view']->total_rows;
  /*
  $items_per_page = $vars['view']->exposed_data['items_per_page'];
  
  if($items_per_page !== 'All' && $total_rows > $items_per_page) {
    $show_all_string = $vars['view']->current_display === 'events' ? 'Show all my events (@count)' : 'Show all my recreational activities (@count)';
    $show_all_text = ka_t($show_all_string, array('@count' => $total_rows));
    $vars['show_all'] = sprintf('<center><a href="" class="show-all link after-arrow_down">%s</a></center>', $show_all_text);
  } else if($total_rows > 5) {
    $show_less_text = ka_t('Show less (@count)', array('@count' => $total_rows));
    $vars['show_less'] = sprintf('<center><a href="" class="show-less link after-arrow_up">%s</a></center>', $show_less_text);
  }
  */
}

function kultuuriaken_views_load_more_pager__organizer_tables($vars) {
  global $pager_page_array, $pager_total, $pager_total_items;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  $parameters['attributes'] = array(
    'class' => 'stuff',
  );

  $pager_classes = array('pager', 'pager-load-more', );

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])) . ' (' . $pager_total_items[0] . ')',
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
      'attributes' => array('class' => 'link after-arrow_down'),
    )
  );
  
  if (empty($li_next)) {
    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
    $pager_classes[] = 'pager-load-more-empty';
  }
  // Compatibility with tao theme's pager
  elseif (is_array($li_next) && isset($li_next['title'], $li_next['href'], $li_next['attributes'], $li_next['query'])) {
    $li_next = l($li_next['title'], $li_next['href'], array('attributes' => $li_next['attributes'], 'query' => $li_next['query']));
  }
  
  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    return theme('pager_show_all',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => $pager_classes),
      )
    );
  }
}

function kultuuriaken_views_load_more_pager($vars) {
  global $pager_page_array, $pager_total, $pager_total_items;
  
  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  $total = ($pager_total_items[0]) - ($pager_page_array[0]) -5;

  $parameters['attributes'] = array(
    'class' => 'stuff',
  );

  $pager_classes = array('pager', 'pager-load-more', );

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])) . ' (' . $total . ')',
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
      'attributes' => array('class' => 'link after-arrow_down'),
    )
  );

  if (empty($li_next)) {
    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
    $pager_classes[] = 'pager-load-more-empty';
  }
  // Compatibility with tao theme's pager
  elseif (is_array($li_next) && isset($li_next['title'], $li_next['href'], $li_next['attributes'], $li_next['query'])) {
    $li_next = l($li_next['title'], $li_next['href'], array('attributes' => $li_next['attributes'], 'query' => $li_next['query']));
  }
  
  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );

    return theme('pager_show_all',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => $pager_classes),
      )
    );
  }
}

function kultuuriaken_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  $attributes = isset($variables['attributes']) ? $variables['attributes'] : NULL;
  global $pager_page_array, $pager_total;
  $output = '';
  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters, 'attributes' => $attributes));
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters, 'attributes' => $attributes));
    }
  }
  
  return $output;
}

function kultuuriaken_pager_last($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = isset($variables['attributes']) ? $variables['attributes'] : NULL;
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $output = theme('pager_link', array('text' => $text, 'page_new' => pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), 'element' => $element, 'parameters' => $parameters, 'attributes' => $attributes));
  }
  return $output;
}

function kultuuriaken_pager_show_all($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];
  $attributes['class'][] = 'no-bullets';
  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '<center>';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;

    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</center>';
  return $output;
}

function do_stuff() {
  drupal_set_message('ok');
}