(function ($) {
  Drupal.behaviors.simpleWeather = {
    attach: function (context, settings) {
      
      // Search result autofill
      if(!isEmpty($('[data-id="eventCount"]', context))) 
        $('#eventCount', context).text($('[data-id="eventCount"]', context).text());
      if(!isEmpty($('[data-id="activityCount"]', context))) 
        $('#activityCount', context).text($('[data-id="activityCount"]', context).text());
      
      resetSearch();
      $(document, context).ajaxStop(function() {
        resetSearch();
      });
      
      // Load more pager -- global search
      $('[data-load-more]').click(function(e)
        {
          e.preventDefault();
          var type = $(this).attr('data-load-more');
          
          $('[data-result-row="'+type+'"]:hidden').slice(0, 5).show();
          
          // Hide load more button
          var el_count = $('[data-result-row="'+type+'"]:hidden').length;
          if(el_count === 0) {
            $('[data-load-more="'+type+'"]').remove();
          } else {
            // Update loader count
            $('[data-load-more="'+type+'"] .load-more-count').text(el_count);
          }
        }
      );
      
      // Load more pager -- organizer
      size_li = $("#eventTable tr.eventItem").size();
      x=3;
      $('#eventTable tr.eventItem:lt('+x+')').addClass('visible-row');
      $('#loadMore').click(function (e) {
          e.preventDefault();
          x= (x+5 <= size_li) ? x+5 : size_li;
          $('#eventTable tr.eventItem:lt('+x+')').addClass('visible-row');
          
          if(x >= size_li) {
            $('#loadMore').remove();
          }
      });
     
     if(isEmpty($('#current-weather'))) {
       $.simpleWeather({
      	//location: 'Tartu, Estonia',
      	unit: 'c',
      	woeid: '845805',
      	success: function(weather) {
      	  html = weather.temp + '<sup>°</sup>';
		  		html += '<img src="' + weather.image + '">';
      		$("#current-weather").html(html);
      	}
      });
     }
      
      $('body', context).on('click', "[data-static-target]", function(event){
        event.preventDefault();
        var targetId = $(this, context).attr('data-static-target');
        $('[data-id=' + targetId + ']', context).trigger('click');
      });
      
      $('.click-next', context).click(function(e) {
        e.preventDefault();
        $(this, context).next('button').trigger('click');
      });
      
      $('[data-target-id]').click(function(e) {
          e.preventDefault();
          var targetId = $(this, context).attr('data-target-id');
          var groupId = $(this).attr('data-target-group-id');
          
          if(typeof groupId !== typeof undefined && groupId !== false) {
            $('[data-id=' + targetId + '][data-group-id=' + groupId + ']', context).trigger('click');
          } else {
            $('[data-id=' + targetId + ']', context).trigger('click');
          }
      });
      
      $(document, context).find('#newsletter-modal').click(function(e) {
        e.preventDefault();
        
        var newsletterModal = new tingle.modal({
            footer: false,
            stickyFooter: false,
            closeMethods: ['overlay', 'escape'],
            closeLabel: "Close",
            cssClass: ['tingle-newsletter'],
            onOpen: function() {
            },
            onClose: function() {
                console.log('modal closed');
            },
            beforeClose: function() {
                // here's goes some logic
                // e.g. save content before closing the modal
                return true; // close the modal
            	return false; // nothing happens
            }
        });
        
        newsletterModal.setContent($('#newsletter-form-modal').html());
        $('.tingle-modal select[name="submitted[kategooriad][]"]', context).SumoSelect();
        newsletterModal.open();
        
        $('[data-action="close-modal"]', context).click(function(e) {
          e.preventDefault();
          newsletterModal.close();
        })
      });
      
      function isEmpty( el ){
        return !$.trim(el.html());
      }
      
      function resetSearch() {
        // Search reset button
        $(document, context).find('#resetSearch').click(function(e) {
	        e.preventDefault();
	        $('#views-exposed-form-general-search-events', context).find('input[type=checkbox]').prop('checked', false);
	        $('#views-exposed-form-general-search-events', context).find('input[type=text]').val('');
	        $('#views-exposed-form-general-search-events', context).submit();
        });
        
        // Refresh page after filter change
        $(document, context).find('[name="node_type[event]"], [name="node_type[activity]"]').change(
          function() {
            $('#views-exposed-form-general-search-events', context).submit();
          }
        );
      }
      
    }
  };
})(jQuery);