<div class="inline">
  <div class="row">
     <div class="col-12">
        <ul class="breadcrumbs">
           <li><a href="<?php print url(); ?>"><?php print ka_t('Home page'); ?></a></li>
           <li><span class="before-arrow_right"></span></li>
           <li><a href=""><?php print ka_t('For organisers'); ?></a></li>
        </ul>
     </div><!--/col-12-->
  </div><!--/row-->
  
  <div class="row">
     <div class="col-12">
        <h1><?php print ka_t('For organisers'); ?></h1>
     </div><!--/col-12-->
  </div><!--/row-->

  <?php if(!$logged_in): ?>
      <?php print render($login_form); ?>
  <?php else: ?>
  <?php print $organizer_table_events; ?>
  <?php print $organizer_table_activities; ?>
  
  <?php endif; ?>
         
  <?php print $organizer_faq; ?>
         
</div>