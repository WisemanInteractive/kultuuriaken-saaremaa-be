<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<?php if($show_activity_search): ?>
  <div class="row">
    <div class="col-12">
      <h3><?php print ka_t('Recreational activities'); ?> (<span data-id="activityCount"><?php if(!empty($header)) print $header; else{print '0';} ?></span>)</h3>
      <?php if(isset($empty) && $empty): ?>
        <?php print $empty; ?>
      <?php endif; ?>
    </div><!--/col-12-->
  </div>
  
  <?php if ($rows): ?>
  <div class="row">
    <div class="col-12">
      <div class="list-view">
        <div id="activity-result" class="row vertical-half">
        <?php print $rows; ?>
        </div><!--/row-->
      </div><!--/list-view-->
    </div><!--/col-12-->
  </div>
  <?php endif; ?>
  
  <?php if ($pager): ?>
    
    <div class="row">
      <div class="col-12">
        <center>
          <a href="" class="link after-arrow_down"><?php print ka_t('Load more recreational activities'); ?> (<?php if(!empty($header)) print $header; else{print '0';} ?>)</a>
          <?php print $pager; ?>
        </center>
      </div><!--/col-12-->
    </div>
  <?php endif; ?>
  
  <div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
<?php endif; ?>