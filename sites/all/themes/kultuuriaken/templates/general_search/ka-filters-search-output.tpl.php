
<?php if($params['event_search']): ?>
<div class="row">
  <div class="col-12">
     <h3><?php print t('Events'); ?> (<span data-id="eventCount"><?php print count($node_list['event']); ?></span>)</h3>
  </div><!--/col-12-->
</div>

<div class="row">
  <div class="col-12">
  <?php if(!empty($node_list['event'])): ?>
  <div class="list-view">
    <div class="row vertical-half">
    <?php 
    $count = 0;
    foreach($node_list['event'] as $node): 
    ++$count;
    ?>
    <div class="col-4" data-result-row="event" <?php if($count > 4) print 'style="display:none;"'; ?>>
      <div class="list-item">
        <div class="price"><?php print $node['price']; ?></div>
        <ul class="tags filled">
        <?php foreach($node['tags'] as $tag): ?>
          <li class="tag-<?php print $tag->type; ?>"><a href="<?php print $node['url']; ?>"><?php print $tag->label; ?></a></li>
        <?php endforeach; ?>
        </ul>
        <a href="<?php print $node['url']; ?>" class="image" style="background-image:url(<?php print $node['image']; ?>)">
           <img src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" class="placeholder">
        </a><!--/image-->
        <a href="<?php print $node['url']; ?>" class="details">
           <span class="title"><?php print $node['title']; ?></span>
           <span class="info">
              <p class="before-location"><?php print $node['location']; ?></p>
              <p class="before-calendar"><?php print $node['date']; ?></p>
           </span><!--/info-->
           <p><?php print $node['content']; ?></p>
        </a><!--/details-->
      </div><!--/list-item-->
    </div>
    <?php endforeach; ?>
    </div>
  </div>
  <?php else: ?>
  <h5><?php print t('No results matched your search.'); ?></h5>
  <?php endif; ?>
  </div>
</div>

<?php if(!empty($node_list['event']) && count($node_list['event']) > 5): ?>
<div class="row">
	<div class="col-12">
		<center>
			<ul class="pager pager-load-more no-bullets">
				<li class="pager-next first last">
					<a data-load-more="event" class="link after-arrow_down" href="#loadmore">
					  <?php print t('Load more (<span class="load-more-count">@count</span>)', array('@count' => count($node_list['event']) - 5)); ?>
					</a>
				</li>
			</ul>
		</center>
	</div>
</div>
<?php endif; ?>

<?php endif; ?>

<?php if($params['activity_search']): ?>
<div class="row">
  <div class="col-12">
     <h3><?php print t('Activities'); ?> (<span data-id="activityCount"><?php print count($node_list['activity']); ?></span>)</h3>
  </div><!--/col-12-->
</div>

<div class="row">
  <div class="col-12">
  <?php if(!empty($node_list['activity'])): ?>
  <div class="list-view">
    <div class="row vertical-half">
    <?php 
    $count = 0;
    foreach($node_list['activity'] as $node): 
    ++$count;
    ?>
    <div class="col-4" data-result-row="activity" <?php if($count > 4) print 'style="display:none;"'; ?>>
      <div class="list-item">
        <div class="price"><?php print $node['price']; ?></div>
        <ul class="tags filled">
        <?php foreach($node['tags'] as $tag): ?>
          <li class="tag-<?php print $tag->type; ?>"><a href="<?php print $node['url']; ?>"><?php print $tag->label; ?></a></li>
        <?php endforeach; ?>
        </ul>
        <a href="<?php print $node['url']; ?>" class="image" style="background-image:url(<?php print $node['image']; ?>)">
           <img src="/<?php print path_to_theme() . "/assets/imgs/placeholder-56.gif"; ?>" class="placeholder">
        </a><!--/image-->
        <a href="<?php print $node['url']; ?>" class="details">
           <span class="title"><?php print $node['title']; ?></span>
           <span class="info">
              <p class="before-location"><?php print $node['location']; ?></p>
              <p class="before-calendar"><?php print $node['date']; ?></p>
           </span><!--/info-->
           <p><?php print $node['content']; ?></p>
        </a><!--/details-->
      </div><!--/list-item-->
    </div>
    <?php endforeach; ?>
    </div>
  </div>
  <?php else: ?>
  <h5><?php print t('No results matched your search.'); ?></h5>
  <?php endif; ?>
  </div>
</div>

<?php if(!empty($node_list['activity']) && count($node_list['activity']) > 5): ?>
<div class="row">
	<div class="col-12">
		<center>
			<ul class="pager pager-load-more no-bullets">
				<li class="pager-next first last">
					<a data-load-more="activity" class="link after-arrow_down" href="#">
					  <?php print t('Load more (@count)', array('@count' => count($node_list['activity']) - 5)); ?>
					</a>
				</li>
			</ul>
		</center>
	</div>
</div>
<?php endif; ?>

<?php endif; ?>