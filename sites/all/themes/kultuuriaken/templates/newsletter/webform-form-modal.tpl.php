<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php unset($form['submitted']['kategooriad']['#attributes']['data-plugin']); ?>
<div class="block">
  <div class="row">
    <div class="col-12">
			<span class="pull-right">
			  <a data-action="close-modal" href="" class="link after-close">
			    <?php print ka_t('Close'); ?>
			  </a>
			 </span>
		</div>
  </div>
  <div class="row">
    <div class="col-12">
      <h3><?php print ka_t('Order Notification'); ?></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <?php print ka_t('Choose which categories you want to receive notifications for:'); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="col-12">
      		<div class="form-item">
      		  <?php print render($form['submitted']['kategooriad']); ?>
      		</div><!--/form-item-->
      	</div><!--/col-4-->
      </div>
      <div class="row">
      	<div class="col-12">
      		<div class="form-item">
      			<?php print render($form['submitted']['e_mail']); ?>
      		</div><!--/form-item-->
      	</div><!--/col-5-->
      </div><!--/row-->
      <div class="row">
        <div class="col-12">
      	  <?php print render($form['actions']['submit']); ?>
      	  <a data-action="close-modal" href="" class="link secondary"><?php print ka_t('Cancel'); ?></a>
      	</div><!--/col-4-->
      </div>
    </div>
  </div>
</div>
<?php
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);

  // Print out the preview message if on the preview page.
  if (isset($form['preview_message'])) {
    print '<div class="messages warning">';
    print drupal_render($form['preview_message']);
    print '</div>';
  }

  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above (buttons, hidden elements, etc).
  print drupal_render_children($form);
