<div class="accessibility-settings" data-plugin="accessibility" data-show="accessibility">
   <div class="accessibility-layer" data-toggle="accessibility"></div>
   <form>
   <div class="inline">
      <div class="row">
         <div class="col-3">
            <p><b><?php print ka_t('Text size'); ?></b></p>
            <div class="form-item">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="textSize" value="0">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title"><?php print ka_t('Medium'); ?></span>
               </label>
            </div><!--/form-item-->
            <div class="form-item large">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="textSize" value="1">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title"><?php print ka_t('Large'); ?></span>
               </label>
            </div><!--/form-item-->
            <div class="form-item x-large">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="textSize" value="2">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title"><?php print ka_t('Very large'); ?></span>
               </label>
            </div><!--/form-item-->
         </div><!--/col-3-->
         
         <div class="col-3">
            <p><b><?php print ka_t('Line spacing'); ?></b></p>
            <div class="form-item">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="lineHeight" value="2x">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title">2x</span>
               </label>
            </div><!--/form-item-->
            <div class="form-item large">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="lineHeight" value="4x">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title">4x</span>
               </label>
            </div><!--/form-item-->
            <div class="form-item x-large">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="lineHeight" value="6x">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title">6x</span>
               </label>
            </div><!--/form-item-->
         </div><!--/col-3-->
         
         <div class="col-3">
            <p><b><?php print ka_t('Contrast'); ?></b></p>
            
            <div class="form-item">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="contrast" value="0">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title"><?php print ka_t('Normal'); ?></span>
               </label>
            </div><!--/form-item-->
            
            <div class="form-item high-contrast">
               <label>
                  <span class="customRadio">
                     <input type="radio" name="contrast" value="1">
                     <span class="indicator"></span>
                  </span><!--/customRadio-->
                  <span class="label-title"><?php print ka_t('High contrast (black background, yellow text)'); ?></span>
               </label>
            </div><!--/form-item-->
         </div><!--/col-3-->
      </div><!--/row-->
      
      <div class="row">
         <div class="col-6">
            <a href="javascript:void(0);" class="btn" rel="submit"><?php print ka_t('Apply settings'); ?></a>
            
            <a href="javascript:void(0);" class="btn btn-plain" rel="reset"><?php print ka_t('Restore default'); ?></a>
         </div><!--/col-6-->
      </div><!--/row-->
   </div><!--/inline-->
   </form>
</div>