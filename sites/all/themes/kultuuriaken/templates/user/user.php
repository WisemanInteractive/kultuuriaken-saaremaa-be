<div class="inline">
  <div class="row">
    <div class="col-12">
      <ul class="breadcrumbs">
        <li><a href="<?php print url(); ?>"><?php print ka_t('Home'); ?></a></li>
        <li><span class="before-arrow_right"></span></li>
        <li><a href=""><?php print ka_t('Settings'); ?></a></li>
      </ul>
    </div><!--/col-12-->
  </div><!--/row-->
  <div class="row">
    <div class="col-12">
       <h1><?php print ka_t('Settings'); ?></h1>
    </div><!--/col-12-->
  </div><!--/row-->
         
  <div class="row">
    <div class="col-12">
      <div class="block rounded">
        
        <?php print $edit_account_form; ?>
        
				<div class="row">
          <div class="col-12">
            <hr>
          </div><!--/col-12-->
        </div><!--/row-->
                  
                  
        <div class="row">
          <div class="col-12">
            <h3><?php print ka_t('Deleting your account'); ?></h3>
            <p><?php print ka_t('In order to delete your account, click "Delete my account".'); ?><br>
            <?php print ka_t('This action also removes all events and recreational activities you have created.'); ?></p>
          </div><!--/col-12-->
        </div><!--/row-->
                  
        <div class="row">
          <div class="col-12">
            <?php print $delete_account_form; ?>
          </div><!--/col-12-->
        </div><!--/row-->
                  
      </div><!--/block-->
    </div><!--/col-3-->
  </div><!--/row-->
</div>