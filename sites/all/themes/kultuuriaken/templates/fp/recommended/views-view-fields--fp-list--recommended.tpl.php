<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="list-item">
   <span class="image" style="background-image:url(<?php print $fields['field_main_image']->content; ?>)">
     <?php if(!empty($fields['summary']['tags'])): ?>
        <ul class="tags filled">
        <?php foreach($fields['summary']['tags'] as $tag): ?>
      		 <li class="tag-<?php print $tag->type; ?>"><a href="<?php print $fields['summary']['url']; ?>"><?php print $tag->label; ?></a></li>
      	<?php endforeach; ?>
      	</ul>
      <?php endif; ?>
      <a href="<?php print $fields['summary']['url']; ?>">
        <img alt="placeholder" src="/<?php print path_to_theme() . '/assets/imgs/placeholder-56.gif'; ?>" class="placeholder">
      </a>
   </span><!--/image-->
   <a href="<?php print $fields['summary']['url']; ?>" class="details">
      <span class="title"><?php print $fields['title_field']->content; ?></span>
      <span class="info">
        <?php if(!empty($fields['summary']['location'])): ?>
        <p class="before-location"><?php print check_plain($fields['summary']['location']); ?></p>
        <?php endif; ?>
        <?php if(!empty($fields['summary']['date'])): ?>
        <p class="before-calendar"><?php print check_plain($fields['summary']['date']); ?></p>
        <?php endif; ?>
      </span><!--/info-->
   </a><!--/details-->
</div><!--/list-item-->
