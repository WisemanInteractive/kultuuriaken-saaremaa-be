<div class="row">
  <div class="col-12">
    <a href="" <?php if(!empty($container['preview_path']['#value'])) print 'onclick="window.open(\'' . $container['preview_path']['#value'] .  '\', \'_blank\')"'; ?> data-target="preview-button" class="link secondary after-modul big-icon"><?php print ka_t('Preview'); ?></a>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <?php print render($container['publish_now']); ?>
    <?php print render($container['publish_later']); ?>
    <?php if(isset($container['save_draft_new_2'])): ?>
      <a id="save-as-draft-popup" href="" class="btn btn-disabled half-margin"><?php print ka_t('Save as a draft'); ?></a>
    <?php endif; ?>
    <?php print render($container['delete']); ?>
    <?php print render($container['preview']); ?>
    <?php if($container['#node_type'] === 'event'): ?>
      <a data-target="delete-node" href="" class="link secondary after-trash pull-right btn-height"><?php print ka_t('Delete event'); ?></a>
    <?php else: ?>
      <a data-target="delete-node" href="" class="link secondary after-trash pull-right btn-height"><?php print ka_t('Delete activity'); ?></a>
    <?php endif; ?>
  </div>
</div>

<?php if(isset($container['save_draft_new_2'])): ?>
  <div id="publish" style="display:none">
    <div class="overlay overlay-enter content-loaded" data-close="true" style="display: block;">
      <div class="overlay-inline">
        <div class="close-x" data-close="true"></div>
        <h1>
          <strong><?php print ka_t('Save as draft?');?></strong>
        </h1>
        <?php print variable_get('save_as_draft_popup_message')['value'];?>
        <div class="row">
          <div class="col-12">
            <center>
                <a href="" class="link secondary close-x-link"><?php print ka_t('Back to form'); ?></a>
                <?php print render($container['save_draft_new_2']); ?>
            </center>
          </div><!--/col-12-->
        </div><!--/row-->
      </div>
    </div>
  </div>
<?php endif; ?>