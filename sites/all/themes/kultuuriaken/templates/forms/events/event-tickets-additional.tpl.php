<div class="row">
  <div class="col-2">
    <div class="form-item">
       <div class="form-item_title"><?php print ka_t('Additional information about tickets'); ?>:</div>
    </div><!--/form-item-->
  </div>
  <div class="col-7">
    <?php print render($container['languages']); ?>
  </div>
</div>

<div class="row">
  <div class="col-2"></div>
  <div class="col-7">
    <?php print render($container['add_additional_multilang_link']); ?>
    <?php print render($container['add_additional_multilang']); ?>
  </div>
</div>