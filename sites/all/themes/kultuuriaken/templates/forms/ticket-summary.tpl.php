<div class="row">
  <div class="col-12">
    <h3>3. <?php print ka_t('Information about the event'); ?><a data-change-id="change-ticket-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
  <?php if(!empty($data['errors'])): ?>
    <span class="circle-icon before-exclamation danger"></span>
    <?php else: ?>
    <span class="circle-icon before-tick"></span>
    <?php endif; ?>
    </h3>
  </div>
</div>
<?php foreach($data['event_date_location'] as $delta => $event_date): ?>
<?php if(!is_numeric($delta)) continue; ?>
<div class="row">
  <div class="col-12">
    <div class="block dashed">
      <div class="row">
        <h5 class="no-margin">
          <?php if(!empty($data['errors']['event_date_location'][$delta])): ?>
          <span class="circle-icon before-exclamation danger"><?php print ka_t('Incomplete'); ?></span>
          <?php else: ?>
          <span class="circle-icon before-tick"></span>
          <?php endif; ?>
        </h5>
      </div>
      <div class="row">
        <div class="col-6">
          <p>
            <b><?php print ka_t('Time of the event'); ?>:</b><br>
            <b><?php print ka_t('Start time'); ?></b>: 
            <?php print check_plain($data['event_date_location'][$delta]['date']['start']['field_event_date_from']) . ' ' . check_plain($data['event_date_location'][$delta]['date']['start']['field_event_time_from']); ?>
            <?php if(!empty($data['errors']['event_date_location'][$delta]['date']['start']['field_event_date_from'])): ?>
            <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['date']['start']['field_event_date_from']); ?>
            <?php endif; ?>
            <?php if(!empty($data['errors']['event_date_location'][$delta]['date']['start']['field_event_time_from'])): ?>
            <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['date']['start']['field_event_time_from']); ?>
            <?php endif; ?>
            <br>

            <b><?php print ka_t('End time'); ?></b>: 
            <?php print check_plain($data['event_date_location'][$delta]['date']['end']['field_event_date_to']); ?>
            <?php if(!$data['event_date_location'][$delta]['end_date_unset'] && $data['event_date_location'][$delta]['date']['end']['field_event_time_to'] !== 'unset'): // Hide if the time is unset ?>
              <?php print ' ' . check_plain($data['event_date_location'][$delta]['date']['end']['field_event_time_to']); ?>
            <?php endif; ?>
            <?php if(!empty($data['errors']['event_date_location'][$delta]['date']['end']['field_event_date_to'])): ?>
            <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['date']['end']['field_event_date_to']); ?>
            <?php endif; ?>

            <?php if(!empty($data['errors']['event_date_location'][$delta]['date']['end']['field_event_time_to'])): ?>
              <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['date']['end']['field_event_time_to']); ?>
            <?php endif; ?>
            <br>
          </p>
        </div><!--/col-6-->
        
        <div class="col-6">
          <p>
            <b><?php print ka_t('Place'); ?>:</b><br>
            <b><?php print ka_t('Location'); ?>:</b> <?php print check_plain($data['event_date_location'][$delta]['location']['field_event_location']); ?>
            <?php if(!empty($data['errors']['event_date_location'][$delta]['location']['field_event_location'])): ?>
              <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['location']['field_event_location']); ?>
            <?php endif; ?>
            <br>
            <b><?php print ka_t('Address'); ?>:</b> <?php print check_plain($data['event_date_location'][$delta]['location']['field_event_address']); ?>
            <?php if(!empty($data['errors']['event_date_location'][$delta]['location']['field_event_address'])): ?>
              <?php print ' ' . ka_form_error($data['errors']['event_date_location'][$delta]['location']['field_event_address']); ?>
            <?php endif; ?>
          </p>
        </div><!--/col-6-->
      </div><!--/row-->
    </div><!--/block dashed-->
  </div><!--/col-12-->
</div>
<?php endforeach; ?>

<div class="row">
  <div class="col-12">
    <div class="block dashed">
      <div class="row">
        <div class="col-6">
          <p>
            <b><?php print ka_t('Ticket information'); ?>:</b><br>
            <b><?php print ka_t('Price'); ?></b>: <?php print $ticket_price; ?>
              <?php if(!empty($data['errors']['event_tickets']['price']['field_event_ticket_price'])): ?>
                <?php print ' ' . ka_form_error($data['errors']['event_tickets']['price']['field_event_ticket_price']); ?>
              <?php endif; ?>
              <?php if(!empty($data['errors']['event_tickets']['price']['field_event_ticket_price_from'])): ?>
                <?php print ' ' . ka_form_error($data['errors']['event_tickets']['price']['field_event_ticket_price_from']); ?>
              <?php endif; ?>
              <?php if(!empty($data['errors']['event_tickets']['price']['field_event_ticket_price_to'])): ?>
                <?php print ' ' . ka_form_error($data['errors']['event_tickets']['price']['field_event_ticket_price_to']); ?>
              <?php endif; ?>
            <br>
            <b><?php print ka_t('Ticket availability'); ?></b>: <?php print $ticket_availability; ?><br>
            <?php if($data['event_tickets']['availability']['field_event_ticket_availability'] === 'link'): ?>
            <b><?php print ka_t('Link'); ?></b>: <?php print check_url($data['event_tickets']['availability']['field_event_ticket_link']); ?>
              <?php if(!empty($data['errors']['event_tickets']['availability']['field_event_ticket_link'])): ?>
                <?php print ' ' . ka_form_error($data['errors']['event_tickets']['availability']['field_event_ticket_link']); ?>
              <?php endif; ?>
            <br>
            <?php elseif($data['event_tickets']['availability']['field_event_ticket_availability'] === 'other'): ?>
            <b><?php print ka_t('Other source'); ?></b>: <?php print check_plain($data['event_tickets']['availability']['field_event_ticket_other']); ?>
            <?php if(!empty($data['errors']['event_tickets']['availability']['field_event_ticket_other'])): ?>
              <?php print ' ' . ka_form_error($data['errors']['event_tickets']['availability']['field_event_ticket_other']); ?>
            <?php endif; ?>
            <br>
            <?php endif; ?>
          </p>
        </div><!--/col-6-->

        <div class="col-6">
          <p>
            <b><?php print ka_t('Additional information'); ?>:</b><br>
            <?php if(!empty($data['event_tickets']['additional']['languages'])): ?>
              <?php foreach($data['event_tickets']['additional']['languages'] as $lang => $additional): ?>
                <b><?php print language_get_full_string($lang); ?></b>: <?php print check_plain($data['event_tickets']['additional']['languages'][$lang]); ?><br>
              <?php endforeach; ?>
            <?php endif; ?>
          </p>
        </div><!--/col-6-->
      </div>
    </div>
  </div>
</div>