<div class="col-12">
  <div class="block rounded">
    <?php if(isset($form['event_type'])): ?>
    <div class="row">
      <div class="col-12">
         <h3>1. <?php print ka_t('Recreational activity type'); ?></h3>
      </div><!--/col-12-->
    </div>
    <?php print render($form['event_type']['category']); ?>
    <?php endif; ?>
    
    <?php if(isset($form['introduction'])): ?>
      <?php print render($form['introduction']); ?>
    <?php endif; ?>
    
    <div class="form-item">
      <label>
        <span class="customRadio">
          <?php print render($form['activity_type']['multigroup']); ?>
          <span class="indicator"></span>
        </span><!--/customRadio-->
        <span class="label-title"><?php print ka_t('Multigroup'); ?></span>
      </label>
      <label>
        <span class="customRadio">
          <?php print render($form['activity_type']['single_group']); ?>
          <span class="indicator"></span>
        </span><!--/customRadio-->
        <span class="label-title"><?php print ka_t('Single group'); ?></span>
      </label>
    </div><!--/form-item-->
    
    <?php if(isset($form['groups'])): ?>
      <?php print render($form['groups']); ?>
    <?php endif; ?>
        
    <?php if(isset($form['event_info'])): ?>
    <div class="row">
      <div class="col-12">
        <h3>3. <?php print ka_t('Information about the groups'); ?></h3>
      </div>
    </div>
    <div class="row">
        <div class="col-12">
          <div class="block dashed">
          <?php print render($form['event_info']['event_date_location']); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="block half-dashed">
          <div class="row">
            <div class="col-12">
              <h6><?php print ka_t('Ticket information'); ?></h6>
              <div class="row">
                <div class="col-2">
                  <div class="form-item">
                    <div class="form-item_title input-height"><?php print ka_t('Ticket price'); ?>:*</div>
                  </div><!--/form-item-->
                </div>
                <div class="col-10">
                <?php print render($form['event_info']['event_tickets']['price']); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-2">
                <div class="form-item">
                   <div class="form-item_title input-height"><?php print ka_t('Ticket availability'); ?>:*</div>
                </div><!--/form-item-->
              </div>
                <div class="col-10">
                  <?php print render($form['event_info']['event_tickets']['availability']); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-2">
                  <div class="form-item">
                     <div class="form-item_title"><?php print ka_t('Additional information about tickets'); ?>:</div>
                  </div><!--/form-item-->
                </div>
                <div class="col-7">
                  <?php print render($form['event_info']['event_tickets']['additional']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    
    <?php if(isset($form['pictures_and_videos'])): ?>
    <?php print render($form['pictures_and_videos']['field_video']); ?>
    <?php print render($form['pictures_and_videos']['gallery']); ?>
    <?php endif; ?>
    
    <?php if(isset($form['organizer'])): ?>
    <div class="row">
      <div class="col-12">
         <h3>5. <?php print ka_t('Organiser data'); ?></h3>
      </div><!--/col-12-->
    </div>
    
    <div class="row separator">
      <div class="col-6">
        <?php print render($form['organizer']['left']); ?>
      </div>
      <div class="col-6">
        <?php print render($form['organizer']['right']); ?>
      </div>
    </div>
    
    <?php print render($form['organizer']['registration_info']); ?>
    <?php endif; ?>
    
    <?php if(isset($form['publish_options'])): ?>
    <?php print render($form['publish_options']); ?>
    <?php endif; ?>
    
    <?php if(isset($form['continue'])): ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
  
  </div>
</div>
<?php if(isset($form['preview'])): ?>
  <?php print render($form['preview']); ?>
<?php endif; ?>
<?php print drupal_render_children($form); ?>