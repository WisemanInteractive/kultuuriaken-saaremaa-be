<div class="row">
  <div class="col-8">
    <h3><?php print ka_t('General settings'); ?> </h3>
  </div><!--/col-8-->
  <div class="col-4 text-right">
    <?php if(isset($form['change'])): ?>
    <a id="edit-account-enable" href="" class="link after-edit secondary"><?php print ka_t('Change'); ?></a>
    <?php print render($form['change']); ?>
    <?php endif; ?>
  </div><!--/col-4-->
</div>

<table class="infoTable">
  <tbody>
    <tr>
      <td><?php print ka_t('Name'); ?><?php if(isset($form['name'])): ?>*<?php endif; ?></td>
      <?php if(isset($form['name'])): ?>
      <td><?php print render($form['name']); ?></td>
      <?php else: ?>
      <td><?php print check_plain($user_data['name']); ?></td>
      <?php endif; ?>
    </tr>
    <tr>
      <td><?php print ka_t('Organisation'); ?></td>
      <?php if(isset($form['institution'])): ?>
      <td><?php print render($form['institution']); ?></td>
      <?php else: ?>
      <td><?php print check_plain($user_data['institution']); ?></td>
      <?php endif; ?>
    </tr>
    <tr>
      <td><?php print ka_t('E-Mail'); ?><?php if(isset($form['mail'])): ?>*<?php endif; ?></td>
      <?php if(isset($form['mail'])): ?>
      <td><?php print render($form['mail']); ?></td>
      <?php else: ?>
      <td><?php print check_plain($user_data['mail']); ?></td>
      <?php endif; ?>
    </tr>
    <tr>
      <td><?php print ka_t('Phone'); ?></td>
      <?php if(isset($form['phone'])): ?>
      <td><?php print render($form['phone']); ?></td>
      <?php else: ?>
      <td><?php print check_plain($user_data['phone']); ?></td>
      <?php endif; ?>
    </tr>
    <tr>
      <td colspan="2"><?php print render($form['save_changes']); ?></td>
    </tr>
  </tbody>
</table>
<?php print drupal_render_children($form); ?>