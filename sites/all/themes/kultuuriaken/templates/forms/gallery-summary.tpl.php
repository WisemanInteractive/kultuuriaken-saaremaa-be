
<div class="row">
  <div class="col-12">
    <h3>4. <?php print ka_t('Event photos and videos'); ?> <a data-change-id="change-gallery-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
    <?php if(!empty($data['errors'])): ?>
    <span class="circle-icon before-exclamation danger"></span>
    <?php else: ?>
    <span class="circle-icon before-tick"></span>
    <?php endif; ?>
    </h3>
  </div>
</div>

<div class="row">
  <div class="col-6">
     <h5><?php print ka_t('Photos'); ?></h5>
     
     
      <?php if(!empty($data['gallery']['image_preview']['images'])): 
      $gallery_chunks = array_chunk($data['gallery']['image_preview']['images'], 3);
      foreach($gallery_chunks as $gallery_chunk): ?>
      <div class="row pull-up">
        <?php foreach($gallery_chunk as $image): 
        $image = is_array($image) ? reset($image) : $image;
        ?>
        <div class="col-3">
          <a target="_blank" href="<?php print file_create_url($image->uri); ?>" class="image" style="background-image:url(<?php print image_style_url('gallery_thumbnail', $image->uri); ?>)"><img src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>"></a>
        </div><!--/col-3-->
        <?php endforeach; ?>
      </div><!--/row-->
      <?php endforeach; ?>
      <?php endif; ?>
      
      <?php if(!empty($data['errors']['gallery'])): ?>
      <div class="row pull-up">
        <div class="col-12">
          <span class="red-text"><?php print ka_form_error($data['errors']['gallery']); ?></span>
        </div>
      </div>
      <?php endif; ?>
     
  </div><!--/col-6-->
  
  <div class="col-6">
     <h5><?php print ka_t('Videos'); ?></h5>
     
     <div class="row pull-up">
        <?php if(!empty($data['video_parsed'])): ?>
        <?php foreach($data['video_parsed'] as $lang => $video):
        if(empty($video) || empty($data['field_video'][$lang])) continue;
        ?>
        <div class="col-3">
        <a target="_blank" href="<?php print file_create_url($video->uri); ?>" class="image before-play" style="background-image:url(<?php print video_thumbnail_url($video->uri); ?>">
          <img alt="placeholder" src="/<?php print path_to_theme() . "/assets/imgs/placeholder-1.gif"; ?>">
        </a>
        </div><!--/col-3-->
        <?php endforeach; ?>
        <?php endif; ?>
     </div><!--/row-->
  </div><!--/col-6-->
</div>

<div class="row">
  <div class="col-12">
    
  </div>
</div>