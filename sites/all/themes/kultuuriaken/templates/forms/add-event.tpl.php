<div class="col-12">
  <div class="block rounded">
    <?php if(isset($form['step_1'])): ?>
    <div class="row">
      <div class="col-12">
        <?php if($form['#node_type'] === 'event'): ?>
        <h3>1. <?php print ka_t('Event description'); ?></h3>
        <?php else: ?>
        <h3>1. <?php print ka_t('Recreational activity type'); ?></h3>
        <?php endif; ?>
      </div><!--/col-12-->
    </div>
    <?php print render($form['step_1']); ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
    <?php if(isset($form['summary'][1])): ?>
    <?php print render($form['summary']['change_1']); ?>
    <?php print render($form['summary'][1]); ?>
    <?php endif; ?>
    <hr>
    
    <?php if(isset($form['summary'][2])): ?>
    <?php print render($form['summary']['change_2']); ?>
    <?php print render($form['summary'][2]); ?>
    <?php else: ?>
    <div class="row">
      <div class="col-12">
        <?php if($form['#node_type'] === 'event'): ?>
        <h3>2. <?php print ka_t('Event type'); ?></h3>
        <?php else: ?>
        <h3>2. <?php print ka_t('Recreational activity description'); ?></h3>
        <?php endif; ?>
      </div><!--/col-12-->
    </div>
    <?php endif; ?>
    <?php if(isset($form['step_2'])): ?>
      <?php if($form['#node_type'] === 'event'): ?>
        <?php print render($form['step_2']['category']); ?>
        <div class="row">
          <div class="col-10">
            <?php print render($form['step_2']['field_target_groups']); ?>
          </div>
        </div>
      <?php else: ?>
        <?php print render($form['step_2']); ?>
      <?php endif; ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
    <hr>
    
    <?php if(isset($form['summary'][3])): ?>
    <?php print render($form['summary']['change_3']); ?>
    <?php print render($form['summary'][3]); ?>
    <?php else: ?>
    <div class="row">
      <div class="col-12">
        <?php if($form['#node_type'] === 'event'): ?>
        <h3>3. <?php print ka_t('Information about the event'); ?></h3>
        <?php else: ?>
        <h3>3. <?php print ka_t('Information about the recreational activity'); ?></h3>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if(isset($form['step_3'])): ?>
      <?php if($form['#node_type'] === 'event'): ?>
        <div class="row">
        <div class="col-12">
          <div class="block dashed">
          <?php print render($form['step_3']['event_date_location']); ?>
        </div>
      </div>
    </div>
        <div class="row">
      <div class="col-12">
        <div class="block half-dashed">
          <div class="row">
            <div class="col-12">
              <h6><?php print ka_t('Ticket information'); ?></h6>
              <div class="row">
                <div class="col-2">
                  <div class="form-item">
                    <div class="form-item_title input-height"><?php print ka_t('Ticket price'); ?>:*</div>
                  </div><!--/form-item-->
                </div>
                <div class="col-10">
                <?php print render($form['step_3']['event_tickets']['price']); ?>
                </div>
              </div>
              <div class="row">
                <div class="col-2">
                  <div class="form-item">
                     <div class="form-item_title input-height"><?php print ka_t('Ticket availability'); ?>:</div>
                  </div><!--/form-item-->
                </div>
                <div class="col-10">
                  <?php print render($form['step_3']['event_tickets']['availability']); ?>
                </div>
              </div>
              <?php print render($form['step_3']['event_tickets']['additional']); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php else: ?>
        <?php print render($form['step_3']); ?>
      <?php endif; ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
    <hr>
    <?php if(isset($form['summary'][4])): ?>
    <?php print render($form['summary']['change_4']); ?>
    <?php print render($form['summary'][4]); ?>
    <?php else: ?>
    <div class="row">
      <div class="col-12">
        <?php if($form['#node_type'] === 'event'): ?>
        <h3>4. <?php print ka_t('Event photos and videos'); ?></h3>
        <?php else: ?>
        <h3>4. <?php print ka_t('Photos and videos'); ?></h3>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>
    
    <?php if(isset($form['step_4'])): ?>
    <?php print render($form['step_4']['field_video']); ?>
    <?php print render($form['step_4']['gallery']); ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
    <hr>
    
    <?php if(isset($form['summary'][5])): ?>
    <?php print render($form['summary']['change_5']); ?>
    <?php print render($form['summary'][5]); ?>
    <?php else: ?>
    <div class="row">
      <div class="col-12">
         <h3>5. <?php print ka_t('Organiser data'); ?></h3>
      </div><!--/col-12-->
    </div>
    <?php endif; ?>
    
    <?php if(isset($form['step_5'])): ?>
    <div class="row separator">
      <div class="col-6">
        <?php print render($form['step_5']['left']); ?>
      </div>
      <div class="col-6">
        <?php print render($form['step_5']['right']); ?>
      </div>
    </div>
    <?php if(isset($form['step_5']['registration'])): ?>
      <?php print render($form['step_5']['registration']); ?>
    <?php endif; ?>
    <?php print render($form['continue']); ?>
    <?php endif; ?>
    
    <?php if(isset($form['publish_options'])): ?>
    <hr>
    <?php print render($form['publish_options']); ?>
    <?php endif; ?>
    
  </div>
</div>
<?php if(isset($form['preview'])): ?>
  <?php //print render($form['preview']); ?>
<?php endif; ?>
<?php print drupal_render_children($form); ?>