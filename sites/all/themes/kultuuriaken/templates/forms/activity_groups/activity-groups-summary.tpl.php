<div class="row">
  <div class="col-12">
     <h3>3. <?php print ka_t('Information about the recreational activity'); ?>
     <a data-change-id="change-activity-group-summary" href="" class="link secondary before-edit pull-right"><?php print ka_t('Change'); ?></a>
     </h3>
  </div><!--/col-12-->
</div>

<?php foreach($summary['#data']['groups'] as $group_id => $group): 
  if(!empty($group['remove_group_confirmed']) && $group['remove_group_confirmed']) continue;
?>
  <?php include 'activity-group.tpl.php'; ?>
<?php endforeach; ?>