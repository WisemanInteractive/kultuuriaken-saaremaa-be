<div class="row">
  <div class="col-12">
    <h6><?php print ka_t('Registration information'); ?>:</h6>
  </div><!--/col-12-->
</div>

<div class="row">
                     
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title">&nbsp;</div>
      <?php print render($container['field_registration_types']['local']); ?>
    </div><!--/form-item-->
  </div><!--/col-2-->
  
  <div class="col-4">
    <div class="form-item padding-right">
      <div class="form-item_title"><?php print ka_t('Address'); ?>:</div>
      <?php print render($container['field_registration_local']); ?>
    </div><!--/form-item-->
  </div><!--/col-4-->
  
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title">&nbsp;</div>
        <?php print render($container['field_registration_types']['phone']); ?>
    </div><!--/form-item-->
  </div><!--/col-2-->
  
  <div class="col-4">
    <div class="form-item">
      <div class="form-item_title"><?php print ka_t('Phone nr.'); ?>:</div>
      <?php print render($container['field_registration_phone']); ?>
    </div><!--/form-item-->
  </div><!--/col-4-->
                     
</div>

<div class="row pull-up">
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title">&nbsp;</div>
        <?php print render($container['field_registration_types']['email']); ?>
    </div><!--/form-item-->
  </div><!--/col-2-->
  
  <div class="col-4">
    <div class="form-item padding-right">
      <div class="form-item_title"><?php print ka_t('E-mail'); ?>:</div>
      <?php print render($container['field_registration_email']); ?>
    </div><!--/form-item-->
  </div><!--/col-4-->
   
  <div class="col-2">
    <div class="form-item">
      <div class="form-item_title">&nbsp;</div>
        <?php print render($container['field_registration_types']['internet']); ?>
    </div><!--/form-item-->
  </div><!--/col-2-->
   
  <div class="col-4">
    <div class="form-item">
      <div class="form-item_title"><?php print ka_t('URL'); ?>:</div>
      <?php print render($container['field_registration_internet']); ?>
    </div><!--/form-item-->
  </div><!--/col-4-->
   
</div>
<?php //print render($container); ?>