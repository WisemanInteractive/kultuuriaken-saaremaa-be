<div class="inline">
         
  <div class="row sm-hide">
    <div class="col-12">
      <ul class="breadcrumbs">
        <li><a href="<?php print url('<front>'); ?>"><?php print ka_t('Home page'); ?></a></li>
        <li><span class="before-arrow_right"></span></li>
        <li><a href=""><?php print ka_t('Calendar'); ?></a></li>
      </ul>
    </div><!--/col-12-->
  </div><!--/row-->
  
  <div class="row">
    <div class="col-6">
      <h1><?php print ka_t('My calendar'); ?></h1>
    </div><!--/col-6-->
    <div class="col-6 text-right">
      <a href="<?php print url('calendar/export'); ?>" class="link after-calendar"><?php print ka_t('Add to calendar'); ?></a>
      <a target="_blank" href="<?php print url('calendar/print'); ?>" class="btn btn-inactive after-calendar"><?php print ka_t('Print calendar'); ?></a>
    </div><!--/col-6-->
  </div><!--/row-->
  
  <div class="row">
    <div class="col-12">
       
       <!-- Calendar template inside -->
      <div class="calendar" data-plugin="calendar" data-json="<?php print url('/calendar/json'); ?>" data-template="<?php print url('/calendar/template'); ?>" data-eventremove="<?php print url('calendar/remove'); ?>/{id}" data-removelabel="<?php print ka_t('Remove'); ?>">
          
      </div><!--/calendar-->
       
    </div><!--/col-12-->
  </div><!--/row-->
         
</div>